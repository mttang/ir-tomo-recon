import java.io.*;
import java.awt.*;
import java.text.DecimalFormat;
import java.util.*;
import java.util.List;

import ij.*;
import ij.process.*;
import ij.gui.*;
import ij.plugin.*;
//import ij.macro.*;
import ij.io.*;

import javax.swing.filechooser.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.*;

import ncsa.hdf.object.Dataset;
import ncsa.hdf.object.HObject;

import java.io.FilenameFilter;
import java.io.File;
import java.util.regex.*;
//import java.awt.event.ItemEvent;
//import java.awt.event.ItemListener;
import java.awt.Checkbox;
import java.awt.TextField;





//import ij.measure.*;
//import static ij.plugin.Slicer.*;
import ncsa.hdf.hdf5lib.exceptions.HDF5Exception;
import ncsa.hdf.object.Attribute;
//import ncsa.hdf.object.Dataset;
import ncsa.hdf.object.Datatype;
//import ncsa.hdf.object.FileFormat;
import ncsa.hdf.object.Group; // the common object package
//import ncsa.hdf.object.HObject;
import ncsa.hdf.object.h5.H5File;// the HDF5 implementation

public class reconstruction_ implements PlugIn {
	String rootName;
	String directory;
	String pluginPath;
	
	int nangles; //# of degrees
	int iterations;
	int bands; //# of wavelengths
	int xwidth;
	int yheight;
	float increment;
	float orbit;
	boolean isFloat = false;

	ImageStack imgStack;
	ImagePlus fullStack;
	int imagesLoaded = 0;
	int firstImage;
	int lastImage;
	int imageIncrement=1; 
	Map<String, Object> waveMap = new HashMap<String, Object>();
	
	float[] waveFloatList;
	String[] waveStringList;
	float waveIncrement;
	List<Group> degreeList;
	String[] degreeChoices;
	
	int[] cropdims = new int[3];
	int[] submitdims = new int[3];
	int[] finaldims = new int[3];

	/*
	 * Main function that Java machine runs first by default. immediately calls run function
	 */
	public static void main(String[] args){
		new reconstruction_().run(null);
	}
	
	/*
	 *Prompts the user to designate an H5 file as input 
	 */
	private String getTheInputName(){
		String input = "";
		JFileChooser fc = new JFileChooser(OpenDialog.getLastDirectory());
		FileNameExtensionFilter filter = new FileNameExtensionFilter("H5 File", "h5 file", "h5", "hdf5");
		fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		fc.setFileFilter(filter);
		fc.setDialogTitle("Select h5 file containing ENVI Image Data and metadata");
		fc.showDialog(null, "Select");
		input = fc.getSelectedFile().getAbsolutePath();
		OpenDialog.setLastDirectory(fc.getSelectedFile().getPath().substring(0,fc.getSelectedFile().getPath().lastIndexOf(Prefs.separator)+1));
		
		if (Menus.getPlugInsPath()==null){
			pluginPath = "C:\\";
		} else
			pluginPath = Menus.getPlugInsPath();
		if(input == null){
			input = "";
		}
		IJ.log(input);

	    return input;
	 }
	
	/*
	 *Gets dataset/image instance from H5 file given a image name (the wavelength) and a list of all degrees
	 */	
	Dataset getDataset(String imageName, List<HObject> memberList){
		Dataset dataset =null;
		for(HObject object: memberList){
			String objectName = object.getName();
			if(objectName.equals(imageName) && object instanceof Dataset){
				dataset = (Dataset) object;
				break;
			}
		}
		return dataset;
	}
	
	
	/*Deprecated - not being used
	 *Creates a full image stack (all wavelengths) of one degree group. Good for testing purposes.
	 */	
	private void makeImageStack(H5File ExFile, Map waveMap, String[] waveList, int degreeNum, int firstImage, int lastImage, int imageIncrement){
		try{
			ExFile.open();
			try{
				//read H5 file to get data from the specified "image name"
				DefaultMutableTreeNode ret = (DefaultMutableTreeNode) ExFile.getRootNode();
				Group root = (Group) ret.getUserObject();
				degreeList = root.getMemberList();

				/*for (int i = 0; i < degreeList.size(); i++){
					if (degreeList.get(i).toString().contains("degrees")){
						//System.out.println(degreeList.get(i).toString());
						degreeMap.put(degreeList.get(i).toString().substring(0,degreeList.get(i).toString().lastIndexOf("degrees")),degreeList.get(i));
						if (degreeList.get(i).toString().substring(0,degreeList.get(i).toString().lastIndexOf("degrees")).contains(".")){
							isFloat = true;
						}
					}
				}*/
				DecimalFormat df = new DecimalFormat("0000.00");

				Group group;
				if(!isFloat){ // if the degree list is integer
					//group = (Group) degreeList.get(degreeList.indexOf(degreeMap.get(Integer.toString((int)degree))));
					group = (Group) degreeList.get((int) (degreeNum));

				} else{
					//group = (Group) degreeList.get(degreeList.indexOf(degreeMap.get(Float.toString(degree))));
					group = (Group) degreeList.get((int) (degreeNum));
				}
				List<HObject> memberList = group.getMemberList();

				for (int i = 0; i < memberList.size(); i++){
					for (int j = 0; j < waveStringList.length; j++){
						if (memberList.get(i).toString().contains(waveStringList[j])){
							waveMap.put(waveStringList[j], memberList.get(i));
						}
					}
				}
				getImageStackFromH5(group, waveMap, waveList, firstImage, lastImage, imageIncrement);
				
	
				
				if(imgStack!=null){
					fullStack = new ImagePlus(degreeList.get(degreeNum).toString()+"degrees", imgStack);
					fullStack.show();
				}else{
					throw new Exception("Couldn't load imageStack from H5 File");
				}
				imgStack = null;
			}catch(HDF5Exception hdf5ex){
				IJ.log(hdf5ex.toString());
				if(imagesLoaded != 0){
					IJ.log("Only able to load "+imagesLoaded+" of "+(int)((lastImage - firstImage + 1)/(imageIncrement))+" images.");
					fullStack = new ImagePlus(degreeList.get(degreeNum).toString()+"degrees", imgStack);
					fullStack.show();
				}
			}
			ExFile.close();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
	}	

	/*Deprecated - not being used
	 *Function complementing makeImageStack to read imagestack from h5 file. Good for testing purposes.
	 */	
	private void getImageStackFromH5(Group group, Map waveMap, String[] waveList, int firstImage, int lastImage, int imageIncrement){
		try{
			List<HObject> memberList = group.getMemberList();
			Dataset dataset = null;
			ImagePlus imp = null;
			IJ.log(waveMap.get(waveList[1]).toString());

			for(int i = firstImage; i < lastImage; i+= imageIncrement){
				HObject object = memberList.get(memberList.indexOf(waveMap.get(waveList[i])));
				if(object instanceof Dataset){
					dataset = (Dataset) object;
					dataset.init();
					long[] dimensions = dataset.getDims(); // dimensions[1] = Height, dimensions[2] = Width
					long[] selected = dataset.getSelectedDims(); //the size in each dimension to select
			        		     
			        if(dimensions.length > 3){
						throw new RuntimeException("Unsure how to handle dataset with number of dimensions = "+dimensions.length);
					}
			        
			        // set the selection arrays
			        
					selected[0] = dimensions[0];
					selected[1] = dimensions[1];
					
					Object data = dataset.getData();
					if(data instanceof byte[]){
						byte[] pixels = (byte[])data;
						imp = new ImagePlus(dataset.getFullName(), new ByteProcessor((short)selected[1], (short)selected[0], pixels));
					}else if(data instanceof short[]){
						short[] pixels = (short[])data;
						imp = new ImagePlus(dataset.getFullName(), new ShortProcessor((short)selected[1], (short)selected[0], pixels, null));
					}else if(data instanceof float[]){
						float[] pixels = (float[])data;
						imp = new ImagePlus(dataset.getFullName(), new FloatProcessor((short)selected[1], (short)selected[0], pixels, null));
					}else{
						int[] pixels = (int[])data;
						imp = new ImagePlus(dataset.getFullName(), new ColorProcessor((short)selected[1], (short)selected[0], pixels));
					}
					dataset.clear();
					if(imgStack == null){
						imgStack = new ImageStack((short)dimensions[1], (short)dimensions[0]);
					}
					imgStack.addSlice(imp.getProcessor());
					imagesLoaded++;
					imp = null;
					IJ.showProgress(imagesLoaded, (int)((lastImage - firstImage + 1)/imageIncrement));
				}
			}
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	/*Deprecated
	 *adds images to imagestack
	 */	
	public void addToStack(String stackTitle, String sliceLabel, ImagePlus imp, int xwidth, int yheight){
		
		ImageProcessor ip = imp.getProcessor();
		ImagePlus oldstack = WindowManager.getImage(stackTitle);
		ImageStack newStack;
		if(oldstack!=null && oldstack.getHeight() == ip.getHeight() && oldstack.getWidth() == ip.getWidth()){
			WindowManager.setTempCurrentImage(oldstack);
			oldstack = IJ.getImage();
			newStack = oldstack.getStack();
		}
		else newStack = new ImageStack(xwidth, yheight);
		newStack.addSlice(ip);
		newStack.setSliceLabel(sliceLabel,newStack.getSize());
		//updates old stack//
		if(oldstack == null)oldstack = new ImagePlus();
		oldstack.setStack(stackTitle,newStack);
		oldstack.setSlice(newStack.getSize());
		oldstack.resetDisplayRange();
		if(newStack.getSize()==1)oldstack.show();
	}
	
	/*
	 *Reads images from h5 file when given degree number (in integers, based on the ordering of the degree groups in the H5 file), 
	 *and the band. Additionally, you can specify if you want to read a specified area of the image (not implemented).
	 */	
	ImagePlus getImageDataFromH5(H5File ExFile, float degreeNum, String band, int xOffset, int yOffset, int xLength, int yLength){
		ImagePlus imp = null;
		try{
			//read H5 file to get data from the specified "image name"
			DefaultMutableTreeNode ret = (DefaultMutableTreeNode) ExFile.getRootNode();
			Group root = (Group) ret.getUserObject();
			degreeList = root.getMemberList();
			
			DecimalFormat df = new DecimalFormat("0000.00");
			
			Group group;
			if(!isFloat){ // if the degree list is integer
				//group = (Group) degreeList.get(degreeList.indexOf(degreeMap.get(Integer.toString((int)degree))));
				group = (Group) degreeList.get((int) (degreeNum));
			} else{
				//group = (Group) degreeList.get(degreeList.indexOf(degreeMap.get(df.format(degree))));
				group = (Group) degreeList.get((int) (degreeNum));
			}
			List<HObject> memberList = group.getMemberList();
			Dataset dataset = getDataset(rootName + "_"+ band, memberList);
			IJ.log("Reading "+"degree: "+degreeNum*increment + ", wavelength: " +band);
			if(dataset!=null){
				dataset.init();
				long[] dimensions = dataset.getDims(); // dimensions[1] = Height, dimensions[2] = Width
				long[] selected = dataset.getSelectedDims(); //the size in each dimension to select
				long[] start = dataset.getStartDims(); // the offset of the selection
				if(dimensions.length > 2){
					throw new RuntimeException("Unsure how to handle dataset with number of dimensions = "+dimensions.length);
				}
				

				// reset the selection arrays
				for (int i=0; i<dimensions.length; i++) {
					start[i] = 0;
					selected[i] = 1;
				}
				start[0] = (yOffset < 0)? 0 : yOffset;
				start[1] = (xOffset < 0)? 0 : xOffset;
				selected[0] = (yLength <= 0)? dimensions[0] : yLength;
				selected[1] = (xLength <= 0)? dimensions[1] : xLength;
				
				Object data = dataset.getData();
				if(data instanceof byte[]){
					byte[] pixels = (byte[])data;
					imp = new ImagePlus(dataset.getFullName(), new ByteProcessor((short)selected[1], (short)selected[0], pixels));
				}else if(data instanceof short[]){
					short[] pixels = (short[])data;
					imp = new ImagePlus(dataset.getFullName(), new ShortProcessor((short)selected[1], (short)selected[0], pixels, null));
				}else if(data instanceof float[]){
					float[] pixels = (float[])data;
					imp = new ImagePlus(dataset.getFullName(), new FloatProcessor((short)selected[1], (short)selected[0], pixels, null));
				}else{
					int[] pixels = (int[])data;
					imp = new ImagePlus(dataset.getFullName(), new ColorProcessor((short)selected[1], (short)selected[0], pixels));
				}
				dataset.clear();
			}
		} catch (HDF5Exception ex){
			ex.printStackTrace();
			IJ.log(ex.getMessage());
			IJ.log(ex.getLocalizedMessage());
		} catch (Exception ex){
			ex.printStackTrace();
		} finally{}
		return imp;
	}
	
	/*
	 *Reads metadata (number of wavelengths/degrees/intervals/image dimensions) from h5 file.
	 */	
	public void getMetaDataFromH5(H5File ExFile){
		try{
			IJ.log("Opening H5 file");
			ExFile.open();
			Map<String, String> inputAttributeMap = new HashMap<String, String>();
			DefaultMutableTreeNode ret = (DefaultMutableTreeNode) ExFile.getRootNode();
			Group root = (Group) ret.getUserObject();
			degreeList = root.getMemberList();
			nangles = 0;

			for (int i = 0; i < degreeList.size(); i++){
				if (degreeList.get(i).toString().contains("degrees")){
					nangles++;
				}
			}
			
			IJ.log("nangles = "+nangles);
			float[] degreeArray = new float[nangles];
			orbit = 0f;
			increment = 0f;
			
			for (int i = 0; i < nangles; i++){
				//System.out.println(degreeList.get(i).toString());
				//degreeMap.put(degreeList.get(i).toString().substring(0,degreeList.get(i).toString().lastIndexOf("degrees")),degreeList.get(i));
				//System.out.println(degreeList.get(i).toString().substring(0,degreeList.get(i).toString().lastIndexOf("degrees")));
				if (degreeList.get(i).toString().substring(0,degreeList.get(i).toString().lastIndexOf("degrees")).contains(".")){
					isFloat = true;
				}
				//to find largest degree as orbit//
				if (Float.parseFloat(degreeList.get(i).toString().substring(0,degreeList.get(i).toString().lastIndexOf("degrees")))-orbit >= 0){
					orbit = Float.parseFloat(degreeList.get(i).toString().substring(0,degreeList.get(i).toString().lastIndexOf("degrees")));
				}
				degreeArray[i] = (Float.parseFloat(degreeList.get(i).toString().substring(0,degreeList.get(i).toString().lastIndexOf("degrees"))));
			}
			
			Arrays.sort(degreeArray);
			degreeChoices = new String[nangles];
			DecimalFormat df = new DecimalFormat("0000.00");
			DecimalFormat df2 = new DecimalFormat("###0.00");

			for (int j = 0; j < nangles; j++){
				degreeChoices[j] = df2.format(degreeArray[j]);
			}
			IJ.log("isFloat = "+isFloat);

			
			IJ.log("orbit = "+orbit);
			
			increment = orbit/(nangles);
			IJ.log("increment = "+increment);
			
			List<HObject> memberList;
			Group group;

			if(!isFloat){
				//group = (Group) degreeList.get(degreeList.indexOf(degreeMap.get(Integer.toString((int)0))));
				group = (Group) degreeList.get((0));

			} else{
				//group = (Group) degreeList.get(degreeList.indexOf(degreeMap.get(df.format(0))));
				group = (Group) degreeList.get((0));

			}
			
			memberList = group.getMemberList();
			Dataset dataset = getDataset(memberList.get(0).toString(),memberList);
			//System.out.printf("\ndatasetName = %s\ngroup.getFullName() = %s\n", dataset.getFullName(), group.getFullName());
			if(dataset!=null){
				dataset.init();
				long[] dimensions = dataset.getDims(); // dimensions[1] = Height, dimensions[2] = Width
				long[] selected = dataset.getSelectedDims(); //the size in each dimension to select
				long[] start = dataset.getStartDims(); // the offset of the selection
				if(dimensions.length > 2){
					throw new RuntimeException("Unsure how to handle dataset with number of dimensions = "+dimensions.length);
				}
				xwidth = dataset.getWidth();
				yheight = dataset.getHeight();
			
				dataset.clear();
			}
			IJ.log("xwidth = "+ xwidth);
			IJ.log("yheight = "+ yheight);
			
			@SuppressWarnings("unchecked")
			List<Attribute> meta = group.getMetadata();
			//IJ.log(Arrays.toString(meta.toArray()));
			for (Attribute attr: meta) {
				if (attr.getType().getDatatypeClass() == Datatype.CLASS_STRING) {
					Object[] vals = (Object[]) attr.getValue();
					waveStringList = ((String)vals[0]).split(",");
					inputAttributeMap.put(attr.getName(), (String) vals[0]);
				} 
				else if (attr.getType().getDatatypeClass() ==  Datatype.CLASS_INTEGER
					&& attr.getType().getDatatypeSize() == Long.SIZE / 8) 
				{
					long[] vals = (long[]) attr.getValue();
					inputAttributeMap.put(attr.getName(), Long.toString(vals[0]));
				} else {
					throw new Exception("Don't know how to read attr " + attr.getName());
				}
			}

			for (int i = 0; i < memberList.size(); i++){
				for (int j = 0; j < waveStringList.length; j++){
					if (memberList.get(i).toString().contains(waveStringList[j])){
						waveMap.put(waveStringList[j], memberList.get(i));
					}
				}
			}
			
			
			//IJ.log(Arrays.toString(memberList.toArray())); //[All attributes from group will be composed into a list and printed as an array]
			bands = memberList.size();
			IJ.log("bands = "+bands);
		
			if (nangles%2>0){
				nangles=nangles-1;
			}
			
			ExFile.close();
		}catch(Exception ex){}finally{}
	}
	
	/*
	 *Wrapper to open h5 file with more control. 
	 */	
	public void open_h5(H5File ExFile){
		try {
			ExFile.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/*
	 *Wrapper to control h5 file with more control.
	 */
	public void close_h5(H5File ExFile){
		try {
			ExFile.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	/*
	 *Run function that provides GUIs step by step for users to interact and select the type of reconstruction they want to perform
	 */
	public void run(String arg) {
		/*
		 * Asks user to specify h5 file input
		 */
		String inputPath = getTheInputName();
		if (inputPath==null) {return;}
		else{
			String directory = inputPath.substring(0,inputPath.lastIndexOf(File.separator));
			rootName = inputPath.substring(inputPath.lastIndexOf(File.separator)+1,inputPath.lastIndexOf("."));
			IJ.log("Getting metadata from H5 file");
			H5File testFile = new H5File(inputPath);
			/*
			 * If h5 file exists, get the metadata of the h5 file
			 */
			getMetaDataFromH5(testFile);
			int area = xwidth*yheight;

			String corrDir = directory+"/corr"+File.separator;
			File outputFolder = new File(corrDir);
			if (!outputFolder.exists()){outputFolder.mkdir();}
			if (!outputFolder.exists()){IJ.showMessage("Couldn't create output directory.");return;}

			String read_dataDir = directory+"read_data";
			read_dataDir = read_dataDir+Prefs.separator;

			//create a file list filter to automatically detect parameters
			String sample_file_format = rootName+"_trans_%04d.fld";

			/*
			 * Create new dialog to ask user whether to search for center of rotation or reconstruct data
			 */
			GenericDialog gd2 = new NonBlockingGenericDialog("Reconstruction");
			gd2.setSize(600,700);
			gd2.centerDialog(true);
			gd2.addHelp("https://sites.google.com/a/lbl.gov/microct/user-resources/reconstruction-plugin-instructions");

			gd2.addStringField("Sample File Pattern (%04d=four-digit integers with zero-padding)",sample_file_format,50);
			gd2.addStringField("Slice(s) (single dash-separated range, or single int)",0+"-"+((int) xwidth-1),10);
			gd2.addMessage("Orbit : "+orbit);
			gd2.addMessage("Angular Increment : "+increment);
			gd2.addCheckbox("Search for the center of rotation",false);
			gd2.addCheckbox("Spectral reconstruction",false);

			TextField file_pattern_field;
			TextField slice_field;
			TextField background_field;

			Checkbox corr_box;
			Checkbox reconstruction_box;
			Vector the_checkboxes2;
			Vector the_strings2;
			the_checkboxes2 = gd2.getCheckboxes();
			corr_box = (Checkbox) the_checkboxes2.get(0);
			reconstruction_box = (Checkbox) the_checkboxes2.get(1);
			the_strings2 = gd2.getStringFields();
			file_pattern_field = (TextField) the_strings2.get(0);
			slice_field  = (TextField) the_strings2.get(1);
		
			float cor;
			String temp;
			gd2.showDialog();
			
			/*
			 * If the dialog is canceled or if user didn't check any boxes, program ends.  
			 */
			
			if (gd2.wasCanceled()){return;}
			else {
				String wavelengthFile = read_dataDir+rootName+"_wavelength.dpt";
				String wavelength, TitleImage = new String();
				int first_wavenumber = 0, last_wavenumber= 0, wavenumber = 0;
				int first_slice, last_slice, steps;
				float cor_low, cor_high, cor_step;
				Choice wavelength_choice;
		
				Pattern dashPattern = Pattern.compile("-");
				String Slice = slice_field.getText();
				String[] temparray2 = dashPattern.split(Slice);
				first_slice = Integer.valueOf(temparray2[0]);
				if (temparray2.length > 1){
					last_slice = Integer.valueOf(temparray2[1]);
				} 
				else {
					last_slice = Integer.valueOf(temparray2[0]);
				}
					sample_file_format = file_pattern_field.getText();
									
				/*
				 * Else if the user checks to search for center of rotation.  Create and show dialog to search center of rotation.
				 */
				if (corr_box.getState() == true){
					GenericDialog gd3 = new NonBlockingGenericDialog("Center of Rotation");
					gd3.setSize(600,700);
					gd3.centerDialog(true);
					gd3.addHelp("https://sites.google.com/a/lbl.gov/microct/user-resources/reconstruction-plugin-instructions");
					gd3.addChoice("Wavelength : ", waveStringList, waveStringList[bands/2]);
					gd3.addNumericField("Center Of Rotation (low)",-3,3);
					gd3.addNumericField("Center Of Rotation (high)",3,3);
					gd3.addNumericField("Center Of Rotation (step)",1,3);

					TextField cor_low_field;
					TextField cor_high_field;
					TextField cor_step_field;
			
					Vector the_numbers3;
					the_numbers3 = gd3.getNumericFields();
					Vector the_strings3;
					the_strings3 = gd3.getChoices();
					wavelength_choice = (Choice) the_strings3.get(0);
					cor_low_field = (TextField) the_numbers3.get(0);
					cor_high_field = (TextField) the_numbers3.get(1);
					cor_step_field = (TextField) the_numbers3.get(2);
					gd3.showDialog();
					
					
					/*
					 * If the dialog is canceled, program ends. Else, run the process.
					 */
					if (gd3.wasCanceled()){return;}
					else {

						wavelength = wavelength_choice.getSelectedItem();
						cor_low = Float.parseFloat(cor_low_field.getText());
						cor_high = Float.parseFloat(cor_high_field.getText());
						cor_step = Float.parseFloat(cor_step_field.getText());
				
						GenericDialog gd31 = new NonBlockingGenericDialog("Confirmation window");
						gd31.setSize(600,700);
						gd31.centerDialog(true);
						gd31.addMessage("Search for the center of rotation.");
						gd31.addMessage("Wavelength : "+wavelength);
						gd31.addMessage("Orbit : "+orbit);
						gd31.addMessage("Angular Increment : "+increment);
						gd31.addMessage("Slice(s) : "+Slice);
						gd31.addMessage("Search Interval : ["+cor_low+" "+cor_high+"]");
						gd31.addMessage("Step : "+cor_step);
						gd31.showDialog();
					
						if (cor_low == cor_high){
							steps = 1;
						} 
						else {
							if (cor_step == 0){steps = 1;} 
							else {steps = (int)((cor_high-cor_low)/cor_step+1);}
						}
						if (gd31.wasCanceled()){return;}
						else {
							open_h5(testFile);

							int last_angle;
							if ((degreeChoices.length-1)%2 != 0){
								last_angle = degreeChoices.length-1;
								setupDims(yheight, first_slice, last_slice, 0, last_angle);
							}
							else {
								last_angle = degreeChoices.length-2;
								setupDims(yheight, first_slice, last_slice, 0, last_angle);
							}
							IJ.log("Calculating center");

							/*
							for(int i=0; i<num_wavelengths;i++){
								if(readfirstdata(wavelengthFile,i).compareTo(wavelength) == 0){
								wavenumber = i;
								} 
							}*/
							wavenumber = Arrays.asList(waveStringList).indexOf(wavelength);
							TitleImage = wavelength + "cm-1";
							onewavenumberreconstruction(wavenumber, testFile, wavelength_choice, corrDir,read_dataDir,rootName,"_trans_%04d.fld",xwidth,yheight,cropdims,submitdims,finaldims,orbit, 0, last_angle,nangles,cor_low,cor_step,steps,first_slice,TitleImage);
							close_h5(testFile);
						}
						
					}
				}
				
				/*
				 * If the user checks to reconstruct datasets.  Create and show dialog for them to select the type of reconstruction they want to perform
				 */
				else if(reconstruction_box.getState() == true){
					GenericDialog gd4 = new NonBlockingGenericDialog("Choose type of reconstruction");
					gd4.setSize(600,700);
					gd4.centerDialog(true);
					gd4.addHelp("https://sites.google.com/a/lbl.gov/microct/user-resources/reconstruction-plugin-instructions");
										gd4.addNumericField("Center Of Rotation",0,3);
					gd4.addCheckbox("Reconstruct one wavelength",false);
					gd4.addCheckbox("Subtraction of two spectral points",false);
					gd4.addCheckbox("Integral of a spectral region",false);
					gd4.addCheckbox("Height of one peak with baseline",false);
					gd4.addCheckbox("Area of one peak with baseline",false);
					gd4.addCheckbox("Reconstruct all wavelengths",false);

					TextField cor_field;
					Checkbox subtraction_box;
					Checkbox integral_box;
					Checkbox wavenumber_box;
					Checkbox allwavenumbers_box;
					Checkbox heightbaseline_box;
					Checkbox areabaseline_box;
					Vector the_checkboxes4;
					Vector the_numbers4;
					the_numbers4 = gd4.getNumericFields();
					the_checkboxes4 = gd4.getCheckboxes();
					wavenumber_box = (Checkbox) the_checkboxes4.get(0);
					subtraction_box = (Checkbox) the_checkboxes4.get(1);
					integral_box = (Checkbox) the_checkboxes4.get(2);
					heightbaseline_box = (Checkbox) the_checkboxes4.get(3);
					areabaseline_box = (Checkbox) the_checkboxes4.get(4);
					allwavenumbers_box = (Checkbox) the_checkboxes4.get(5);
					cor_field = (TextField) the_numbers4.get(0);
					gd4.showDialog();
					
					/*
					 * If the dialog is canceled, program ends. Else, figure out what processes the user wants to perform from what they checked.
					 */
					if (gd4.wasCanceled()){return;}
					else {
						cor = Float.parseFloat(cor_field.getText());
						cor_low = cor;
						cor_high = cor;
						cor_step = 0;
						steps = 1;
						int start_angle = 0, end_angle = degreeChoices.length-1;
						Choice first_wavelength_choice, last_wavelength_choice;
						Choice first_degree_choice, last_degree_choice;
						String first_wavelength, last_wavelength, first_dataFileName, second_dataFileName, third_dataFileName, data1, data2, data3;
						String first_degree, last_degree;
						File first_dataFile, second_dataFile, third_dataFile;
						//int num_lines;
						String[] data1_string, data2_string, data3_string, data1_line_string, data2_line_string, data3_line_string;
						
						/*
						 * If "Reconstruction for one wavelength" was checked, prompt user with dialog to specify one wavelength and angles to reconstruct
						 */
						if (wavenumber_box.getState() == true){
							TextField limited_angle_field;
							GenericDialog gd5 = new NonBlockingGenericDialog("Reconstruction");
							gd5.setSize(600,700);
							gd5.centerDialog(true);
							gd5.addMessage("Reconstruction for one wavelength.");
							gd5.addHelp("https://sites.google.com/a/lbl.gov/microct/user-resources/reconstruction-plugin-instructions");
							gd5.addChoice("Wavelength : ", waveStringList, waveStringList[bands/2]);
							gd5.addChoice("Initial angle : ",degreeChoices, degreeChoices[0]);
							gd5.addChoice("Final angle : ",degreeChoices, degreeChoices[degreeChoices.length-1]);

							Vector the_strings5;
							the_strings5= gd5.getChoices();
							wavelength_choice = (Choice) the_strings5.get(0);
							first_degree_choice = (Choice) the_strings5.get(1);
							last_degree_choice = (Choice) the_strings5.get(2);
							gd5.showDialog();
							/*
							 * If the dialog is canceled, program ends. Else, show a confirmation window
							 */
							if (gd5.wasCanceled()){return;}
							else {
								wavelength = wavelength_choice.getSelectedItem();
								first_degree = first_degree_choice.getSelectedItem();
								last_degree = last_degree_choice.getSelectedItem();

								GenericDialog gd51 = new NonBlockingGenericDialog("Confirmation window");
								gd51.setSize(600,700);
								gd51.centerDialog(true);
								gd51.addMessage("Reconstruction for one wavelength.");
								gd51.addMessage("Wavelength : "+wavelength);
								gd51.addMessage("Angles : "+first_degree+" - "+last_degree);
								gd51.addMessage("Orbit : "+orbit);
								gd51.addMessage("Angular Increment : "+increment);
								gd51.addMessage("Slice(s) : "+Slice);
								gd51.addMessage("Center of Rotation : "+cor);
								gd51.showDialog();
								
								/*
								 * If the dialog is canceled, program ends. Else, run the process.
								 */
								if (gd51.wasCanceled()){return;}
								else {
									open_h5(testFile);
									IJ.log("Processing wavelength reconstruction");

									for (int i = 0; i < degreeChoices.length; i++){
										System.out.printf("degreeChoices[%d] = %s\n", i, degreeChoices[i]);

										if (first_degree == degreeChoices[i]){
											start_angle = i;
										}
										if (last_degree == degreeChoices[i]){
											end_angle = i;
										}
									}
									if ((start_angle + end_angle+1)%2 != 0){
										IJ.log("Total angle dimensions are not even, Aspire reconstruction accepts even angle inputs only");
										if (start_angle == nangles - 2){
											IJ.log("Start angle has been adjusted");
											start_angle -= 1;
										}
										else {
											IJ.log("Last angle has been adjusted");
											end_angle -= 1;
										}
									}
									
									System.out.println("start_angle = " + start_angle);
									System.out.println("end_angle = " + end_angle);

									setupDims(yheight, first_slice, last_slice, start_angle, end_angle);
									/*for(int i=0; i< num_wavelengths;i++){
										if(readfirstdata(wavelengthFile,i).compareTo(wavelength) == 0){
											wavenumber = i;
										} 
									}*/
									wavenumber = Arrays.asList(waveStringList).indexOf(wavelength);
									TitleImage = wavelength+"cm-1";
									onewavenumberreconstruction(wavenumber, testFile, wavelength_choice, corrDir,read_dataDir,rootName,"_trans_%04d.fld",xwidth,yheight,cropdims,submitdims,finaldims,orbit, start_angle, end_angle,nangles,cor,cor_step,steps,first_slice,TitleImage);
									close_h5(testFile);
								}
							}	
						}
						/*
						 * If "Subtraction of two spectral points" was checked, prompt user with dialog to specify two wavelengths and angles to reconstruct
						 */
						else if (subtraction_box.getState() == true){
							GenericDialog gd6 = new NonBlockingGenericDialog("Reconstruction");
							gd6.setSize(600,1000);
							gd6.centerDialog(true);
							gd6.addHelp("https://sites.google.com/a/lbl.gov/microct/user-resources/reconstruction-plugin-instructions");
							gd6.addMessage("Subtraction of two spectral points.");
							gd6.addChoice("Wavelength A: ", waveStringList, waveStringList[bands-1]);
							gd6.addMessage("minus");
							gd6.addChoice("Wavelength B: ", waveStringList, waveStringList[0]);
							gd6.addChoice("Initial angle : ",degreeChoices, degreeChoices[0]);
							gd6.addChoice("Final angle : ",degreeChoices, degreeChoices[degreeChoices.length-1]);
							
							Vector the_strings6;
							the_strings6= gd6.getChoices();
							first_wavelength_choice = (Choice) the_strings6.get(0);
							last_wavelength_choice = (Choice) the_strings6.get(1);
							first_degree_choice = (Choice) the_strings6.get(2);
							last_degree_choice = (Choice) the_strings6.get(3);
							
							gd6.showDialog();
							/*
							 * If the dialog is canceled, program ends. Else, show a confirmation window
							 */
							if (gd6.wasCanceled()){return;}
							else {
								first_wavelength = first_wavelength_choice.getSelectedItem();
								last_wavelength = last_wavelength_choice.getSelectedItem();
								first_degree = first_degree_choice.getSelectedItem();
								last_degree = last_degree_choice.getSelectedItem();
											GenericDialog gd61 = new NonBlockingGenericDialog("Confirmation window");
								gd61.setSize(600,700);
								gd61.centerDialog(true);
								gd61.addMessage("Subtraction : "+first_wavelength+" - "+last_wavelength);
								gd61.addMessage("Angles : "+first_degree+" - "+last_degree);
								gd61.addMessage("Orbit : "+orbit);
								gd61.addMessage("Angular Increment : "+increment);
								gd61.addMessage("Slice(s) : "+Slice);
								gd61.addMessage("Center of Rotation : "+cor);
								gd61.showDialog();
								/*
								 * If the dialog is canceled, program ends. Else, run the process.
								 */
								if (gd61.wasCanceled()){return;}
								else {
									open_h5(testFile);

									for (int i = 0; i < degreeChoices.length; i++){
										if (first_degree == degreeChoices[i]){
											start_angle = i;
										}
										if (last_degree == degreeChoices[i]){
											end_angle = i;
										}
									}
									if ((start_angle + end_angle+1)%2 != 0){
										IJ.log("Total angle dimensions are not even, Aspire reconstruction accepts even angle inputs only");
										if (start_angle == nangles - 2){
											IJ.log("Start angle has been adjusted");
											start_angle -= 1;
										}
										else {
											IJ.log("Last angle has been adjusted");
											end_angle -= 1;
										}
									}
									float[][]ftmp = new float[area][end_angle-start_angle+1];

									setupDims(yheight, first_slice, last_slice, start_angle, end_angle);

									ImagePlus initWave;
									ImagePlus finWave;
									float[] initWavePixels;
									float[] finWavePixels;
									IJ.log("Starting calculation.");
										/*
									first_dataFileName = read_dataDir+rootName+"_"+first_wavelength+"cm-1.dpt";
									first_dataFile = new File(first_dataFileName);
									second_dataFileName = read_dataDir+rootName+"_"+last_wavelength+"cm-1.dpt";
									second_dataFile = new File(second_dataFileName);
									data1 = parse_xrf(first_dataFile);
									data2 = parse_xrf(second_dataFile);
									data1_string = splitPattern.split(data1);
									data2_string = splitPattern.split(data2);
									*/
									String subtractionFileN = "Subtraction_"+first_wavelength+"-"+last_wavelength+"cm-1";
									for(int j=start_angle;j<=end_angle; j++){
										initWave = getImageDataFromH5(testFile, j, first_wavelength_choice.getSelectedItem(), 0, 0, xwidth, yheight);
										initWave.show();
										ImageProcessor initWavePR = initWave.getProcessor();
										initWavePixels = (float[]) initWavePR.getPixels();

										finWave = getImageDataFromH5(testFile, j, last_wavelength_choice.getSelectedItem(), 0, 0, xwidth, yheight);
										ImageProcessor finWavePR = finWave.getProcessor();
										finWave.show();
										finWavePixels = (float[]) finWavePR.getPixels();
										/*
											data1_line_string = splitPatterncomma.split((data1_string[j]));
											data2_line_string = splitPatterncomma.split((data2_string[j]));
										*/
										for (int m=0;m<area-1;m++){
											Float f1 = initWavePixels[m];
											Float f2 = finWavePixels[m];
											if (f2 == null){
												f2 = 1f;
											}
											ftmp[m][j-start_angle] = f1-f2;
										}
										IJ.showProgress(j,end_angle);
									}
										/*
									for(int i=0; i< num_wavelengths;i++){
										if(readfirstdata(wavelengthFile,i).compareTo(first_wavelength) == 0){
											wavenumber = i;
										} 
									}*/
									wavenumber = Arrays.asList(waveStringList).indexOf(first_wavelength);

									onewavenumberreconstruction(wavenumber,corrDir,read_dataDir,rootName,ftmp,"_trans_%04d.fld",xwidth,yheight,cropdims,submitdims,finaldims,orbit, start_angle, end_angle,nangles,cor,cor_step,steps,first_slice,subtractionFileN);
									close_h5(testFile);

								}
							}
						}
						/*
						 * If "Integral of a spectral region" was checked, prompt user with dialog to specify two wavelengths and angles to reconstruct
						 */
						else if (integral_box.getState() == true){
							GenericDialog gd7 = new NonBlockingGenericDialog("Reconstruction");
							gd7.setSize(600,1000);
							gd7.centerDialog(true);
							gd7.addMessage("Integral of a spectral region.");
							gd7.addHelp("https://sites.google.com/a/lbl.gov/microct/user-resources/reconstruction-plugin-instructions");
							gd7.addChoice("Initial Wavelength : ", waveStringList, waveStringList[bands/2]);
							gd7.addChoice("Final Wavelength : ", waveStringList, waveStringList[bands-1]);
							gd7.addChoice("Initial angle : ",degreeChoices, degreeChoices[0]);
							gd7.addChoice("Final angle : ",degreeChoices, degreeChoices[degreeChoices.length-1]);
							
							Vector the_strings7;
							the_strings7= gd7.getChoices();
							first_wavelength_choice = (Choice) the_strings7.get(0);
							last_wavelength_choice = (Choice) the_strings7.get(1);
							first_degree_choice = (Choice) the_strings7.get(2);
							last_degree_choice = (Choice) the_strings7.get(3);
							
							gd7.showDialog();
							/*
							 * If the dialog is canceled, program ends. Else, show a confirmation window
							 */
							if (gd7.wasCanceled()){return;}
							else {
								first_wavelength = first_wavelength_choice.getSelectedItem();
								last_wavelength = last_wavelength_choice.getSelectedItem();
								first_degree = first_degree_choice.getSelectedItem();
								last_degree = last_degree_choice.getSelectedItem();
								
								GenericDialog gd71 = new NonBlockingGenericDialog("Confirmation window");
								gd71.setSize(600,700);
								gd71.centerDialog(true);
								gd71.addMessage("Integral of a spectral region.");
								gd71.addMessage("Interval ["+first_wavelength+" , "+last_wavelength+"]");
								gd71.addMessage("Angles : "+first_degree+" - "+last_degree);
								gd71.addMessage("Orbit : "+orbit);
								gd71.addMessage("Angular Increment : "+increment);
								gd71.addMessage("Slice(s) : "+Slice);
								gd71.addMessage("Center of Rotation : "+cor);
								gd71.showDialog();
								/*
								 * If the dialog is canceled, program ends. Else, run the process.
								 */
								if (gd71.wasCanceled()){return;}
								else {
									open_h5(testFile);

									for (int i = 0; i < degreeChoices.length; i++){
										if (first_degree == degreeChoices[i]){
											start_angle = i;
										}
										if (last_degree == degreeChoices[i]){
											end_angle = i;
										}
									}
									if ((start_angle + end_angle+1)%2 != 0){
										IJ.log("Total angle dimensions are not even, Aspire reconstruction accepts even angle inputs only");
										if (start_angle == nangles - 2){
											IJ.log("Start angle has been adjusted");
											start_angle -= 1;
										}
										else {
											IJ.log("Last angle has been adjusted");
											end_angle -= 1;
										}
									}
									float[][]ftmp = new float[area][end_angle-start_angle+1];

									setupDims(yheight, first_slice, last_slice, start_angle, end_angle);

									IJ.log("Starting calculation.");
									String IntegralFileN = "integral_"+first_wavelength+"-"+last_wavelength+"cm-1";
									first_wavenumber = Arrays.asList(waveStringList).indexOf(first_wavelength);
									last_wavenumber = Arrays.asList(waveStringList).indexOf(last_wavelength);
									int integralRange = Math.abs(last_wavenumber - first_wavenumber )+1;
									
									if(last_wavenumber<first_wavenumber){//switches wavenumbers if you screwed up in the menu//
										int x = last_wavenumber;
										last_wavenumber = first_wavenumber;
										first_wavenumber = x;
									}
									
									for(int v=first_wavenumber; v<=last_wavenumber; v++ ){
										/*
										String s3 = readfirstdata(wavelengthFile, v);
										first_dataFileName = read_dataDir+rootName+"_"+s3+"cm-1.dpt";
										first_dataFile = new File(first_dataFileName);
										data1 =  parse_xrf(first_dataFile);
										*/
										//data1_string = splitPattern.split(data1);
										for(int j=start_angle;j<=end_angle; j++){
											ImagePlus ips = getImageDataFromH5(testFile, j, waveStringList[v], 0, 0, xwidth, yheight);
											ImageProcessor ipr = ips.getProcessor();
											float[] pixels = (float[]) ipr.getPixels();
											ips.close();	
											
											//data1_line_string = splitPatterncomma.split((data1_string[j]));
											for (int h=0;h<area-1;h++){
												Float f1 = pixels[h];
												if(v == last_wavenumber){ftmp[h][j-start_angle] = (f1 + ftmp[h][j-start_angle])/integralRange;}
												else {ftmp[h][j-start_angle] = f1 + ftmp[h][j-start_angle];}
											}
										}
										IJ.showProgress(v,last_wavenumber);
									}
									onewavenumberreconstruction(first_wavenumber,corrDir,read_dataDir,rootName,ftmp, "_trans_%04d.fld",xwidth,yheight,cropdims,submitdims,finaldims,orbit, start_angle, end_angle,nangles,cor,cor_step,steps,first_slice,IntegralFileN);
									close_h5(testFile);

								}
							}
						}
						/*
						 * If "Calculate height of one peak compared to baseline" was checked, prompt user with dialog to specify one peak wavelength and 2 baseline wavelengths and angles to reconstruct
						 */
						else if (heightbaseline_box.getState() == true){
							GenericDialog gd8 = new NonBlockingGenericDialog("Reconstrction");
							gd8.setSize(600,700);
							gd8.centerDialog(true);
							gd8.addMessage("Calculate height of one peak compared to baseline.");
							gd8.addHelp("https://sites.google.com/a/lbl.gov/microct/user-resources/reconstruction-plugin-instructions");
							
							gd8.addChoice("Peak position: Wavelength : ", waveStringList, waveStringList[bands/2]);
							gd8.addMessage("Baseline : Two wavelengths : ");
							gd8.addChoice("first wavelength : ", waveStringList, waveStringList[0]);
							gd8.addChoice("last wavelength : ", waveStringList, waveStringList[bands/2]);
							gd8.addChoice("Initial angle : ",degreeChoices, degreeChoices[0]);
							gd8.addChoice("Final angle : ",degreeChoices, degreeChoices[degreeChoices.length-1]);
							
							Vector the_strings8;
							the_strings8= gd8.getChoices();
							wavelength_choice = (Choice) the_strings8.get(0);
							first_wavelength_choice = (Choice) the_strings8.get(1);
							last_wavelength_choice = (Choice) the_strings8.get(2);
							first_degree_choice = (Choice) the_strings8.get(3);
							last_degree_choice = (Choice) the_strings8.get(4);
							
							gd8.showDialog();
							/*
							 * If the dialog is canceled, program ends. Else, show a confirmation window
							 */
							if (gd8.wasCanceled()){return;}
							else {
								wavelength = wavelength_choice.getSelectedItem();
								first_wavelength = first_wavelength_choice.getSelectedItem();
								last_wavelength = last_wavelength_choice.getSelectedItem();
								first_degree = first_degree_choice.getSelectedItem();
								last_degree = last_degree_choice.getSelectedItem();
								
								GenericDialog gd81 = new NonBlockingGenericDialog("Confirmation window");
								gd81.setSize(600,700);
								gd81.centerDialog(true);
								gd81.addMessage("Height of one peak wih baseline.");
								gd81.addMessage("Peak Position : "+wavelength);
								gd81.addMessage("Baseline : "+first_wavelength+" - "+last_wavelength);
								gd81.addMessage("Angles : "+first_degree+" - "+last_degree);
								gd81.addMessage("Orbit : "+orbit);
								gd81.addMessage("Angular Increment : "+increment);
								gd81.addMessage("Slice(s) : "+Slice);
								gd81.addMessage("Center of Rotation : "+cor);
								gd81.showDialog();
								/*
								 * If the dialog is canceled, program ends. Else, run the process.
								 */
								if (gd81.wasCanceled()){return;}
								else {
									open_h5(testFile);

									for (int i = 0; i < degreeChoices.length; i++){
										if (first_degree == degreeChoices[i]){
											start_angle = i;
										}
										if (last_degree == degreeChoices[i]){
											end_angle = i;
										}
									}
									if ((start_angle + end_angle+1)%2 != 0){
										IJ.log("Total angle dimensions are not even, Aspire reconstruction accepts even angle inputs only");
										if (start_angle == nangles - 2){
											IJ.log("Start angle has been adjusted");
											start_angle -= 1;
										}
										else {
											IJ.log("Last angle has been adjusted");
											end_angle -= 1;
										}
									}
									float[][]ftmp = new float[area][end_angle-start_angle+1];

									setupDims(yheight, first_slice, last_slice, start_angle, end_angle);

									IJ.log("Starting calculation.");
									/*
									first_dataFileName = read_dataDir+rootName+"_"+first_wavelength+"cm-1.dpt";
									first_dataFile = new File(first_dataFileName);
									second_dataFileName = read_dataDir+rootName+"_"+last_wavelength+"cm-1.dpt";
									second_dataFile = new File(second_dataFileName);
									data1 = parse_xrf(first_dataFile);
									data2 = parse_xrf(second_dataFile);
									data1_string = splitPattern.split(data1);
									data2_string = splitPattern.split(data2);
									third_dataFileName = read_dataDir+rootName+"_"+wavelength+"cm-1.dpt";
									third_dataFile = new File(third_dataFileName);
									data3 = parse_xrf(third_dataFile);
									data3_string = splitPattern.split(data3);
									*/
									/*
									for(int i=0; i<num_wavelengths; i++){
										if(readfirstdata(wavelengthFile,i).compareTo(first_wavelength) == 0){first_wavenumber = i;}
										if(readfirstdata(wavelengthFile,i).compareTo(last_wavelength) == 0){last_wavenumber = i;}
										if(readfirstdata(wavelengthFile,i).compareTo(wavelength) == 0){wavenumber = i;}
									}
									*/
										
									first_wavenumber = Arrays.asList(waveStringList).indexOf(first_wavelength);
									last_wavenumber = Arrays.asList(waveStringList).indexOf(last_wavelength);
									wavenumber = Arrays.asList(waveStringList).indexOf(wavelength);

									int y = wavenumber / (last_wavenumber - first_wavenumber);
									for(int j=start_angle;j<=end_angle; j++){
										/*
										data1_line_string = splitPatterncomma.split((data1_string[j]));
										data2_line_string = splitPatterncomma.split((data2_string[j]));
										data3_line_string = splitPatterncomma.split((data3_string[j]));
										*/
										ImagePlus WaveOne;
										ImagePlus WaveTwo;
										ImagePlus PeakWave;
										float[] PixelArrayOne;
										float[] PixelArrayTwo;
										float[] PeakPixelArray;
										WaveOne = getImageDataFromH5(testFile, j, first_wavelength_choice.getSelectedItem(), 0, 0, xwidth, yheight);
										ImageProcessor WaveOnePR = WaveOne.getProcessor();
										PixelArrayOne = (float[]) WaveOnePR.getPixels();
										WaveOne.close();	

										WaveTwo = getImageDataFromH5(testFile, j, last_wavelength_choice.getSelectedItem(), 0, 0, xwidth, yheight);
										ImageProcessor WaveTwoPR = WaveTwo.getProcessor();
										PixelArrayTwo = (float[]) WaveTwoPR.getPixels();
										WaveTwo.close();	
											
										PeakWave = getImageDataFromH5(testFile, j, last_wavelength_choice.getSelectedItem(), 0, 0, xwidth, yheight);
										ImageProcessor PeakWavePR = PeakWave.getProcessor();
										PeakPixelArray = (float[]) PeakWavePR.getPixels();
										PeakWave.close();
											
										for (int m=0;m<area-1;m++){
											Float f1 = PixelArrayOne[m];
											Float f2 = PixelArrayTwo[m];
											Float f3 = PeakPixelArray[m];
											ftmp[m][j-start_angle] = f3-((f2-f1)*y);
											if(ftmp[m][j-start_angle]>6f){ftmp[m][j-start_angle]=6f;}
											else if(ftmp[m][j-start_angle]<-6f){ftmp[m][j-start_angle]=-6f;}
										}	
										IJ.showProgress(j,end_angle);
									}
									String peakbaselineFileN = "peak_"+wavelength+"_with baseline_"+first_wavelength+"-"+last_wavelength+"cm-1";
									onewavenumberreconstruction(wavenumber,corrDir,read_dataDir,rootName,ftmp, "_trans_%04d.fld",xwidth,yheight,cropdims,submitdims,finaldims,orbit, start_angle, end_angle,nangles,cor,cor_step,steps,first_slice,peakbaselineFileN);
									close_h5(testFile);
								}
							}
						}
						/*
						 * If "Area of one peak with baseline" was checked, prompt user with dialog to specify 2 peak wavelengths and 2 base wavelengths and angles to reconstruct
						 */
						else if (areabaseline_box.getState() == true){
							GenericDialog gd9 = new NonBlockingGenericDialog("Reconstruction");
							gd9.setSize(600,700);
							gd9.centerDialog(true);
							gd9.addHelp("https://sites.google.com/a/lbl.gov/microct/user-resources/reconstruction-plugin-instructions");
							gd9.addMessage("Area of one peak with baseline.");
							gd9.addMessage("Peak region : ");
							gd9.addChoice("first wavelength : ", waveStringList, waveStringList[0]);
							gd9.addChoice("last wavelength : ", waveStringList, waveStringList[bands-1]);
							gd9.addMessage("Baseline : ");
							gd9.addChoice("first wavelength : ", waveStringList, waveStringList[0]);
							gd9.addChoice("last wavelength : ", waveStringList, waveStringList[bands-1]);
							gd9.addChoice("Initial angle : ",degreeChoices, degreeChoices[0]);
							gd9.addChoice("Final angle : ",degreeChoices, degreeChoices[degreeChoices.length-1]);
							
							Vector the_strings9;
							the_strings9 = gd9.getChoices();
							Choice first_reconstruction_wavelength_choice = (Choice) the_strings9.get(0);
							Choice last_reconstruction_wavelength_choice = (Choice) the_strings9.get(1);
							first_wavelength_choice = (Choice) the_strings9.get(2);
							last_wavelength_choice = (Choice) the_strings9.get(3);
							first_degree_choice = (Choice) the_strings9.get(4);
							last_degree_choice = (Choice) the_strings9.get(5);
							
							gd9.showDialog();
							/*
							 * If the dialog is canceled, program ends. Else, show a confirmation window
							 */
							if (gd9.wasCanceled()){return;}
							else {
								String reconstruction_first_wavelength = first_reconstruction_wavelength_choice.getSelectedItem();
								String reconstruction_last_wavelength = last_reconstruction_wavelength_choice.getSelectedItem();
								first_wavelength = first_wavelength_choice.getSelectedItem();
								last_wavelength = last_wavelength_choice.getSelectedItem();
								first_degree = first_degree_choice.getSelectedItem();
								last_degree = last_degree_choice.getSelectedItem();
								
								GenericDialog gd91 = new NonBlockingGenericDialog("Confirmation window");
								gd91.setSize(600,700);
								gd91.centerDialog(true);
								gd91.addMessage("Area of one peak with baseline.");
								gd91.addMessage("Peak Region : "+reconstruction_first_wavelength+" - "+reconstruction_last_wavelength);
								gd91.addMessage("Baseline : "+first_wavelength+" - "+last_wavelength);
								gd91.addMessage("Angles : "+first_degree+" - "+last_degree);
								gd91.addMessage("Orbit : "+orbit);
								gd91.addMessage("Angular Increment : "+increment);
								gd91.addMessage("Slice(s) : "+Slice);
								gd91.addMessage("Center of Rotation : "+cor);
								gd91.showDialog();
								/*
								 * If the dialog is canceled, program ends. Else, run the process.
								 */
								if (gd91.wasCanceled()){return;}
								else {
									open_h5(testFile);

									for (int i = 0; i < degreeChoices.length; i++){
										if (first_degree == degreeChoices[i]){
											start_angle = i;
										}
										if (last_degree == degreeChoices[i]){
											end_angle = i;
										}
									}
									if ((start_angle + end_angle+1)%2 != 0){
										IJ.log("Total angle dimensions are not even, Aspire reconstruction accepts even angle inputs only");
										if (start_angle == nangles - 2){
											IJ.log("Start angle has been adjusted");
											start_angle -= 1;
										}
										else {
											IJ.log("Last angle has been adjusted");
											end_angle -= 1;
										}
									}
									float[][]ftmp = new float[area][end_angle-start_angle+1];

									setupDims(yheight, first_slice, last_slice, start_angle, end_angle);

									IJ.log("Starting calculation.");
									int reconstruction_first_wavenumber = Arrays.asList(waveStringList).indexOf(reconstruction_first_wavelength);
									int reconstruction_last_wavenumber = Arrays.asList(waveStringList).indexOf(reconstruction_last_wavelength);
									first_wavenumber = Arrays.asList(waveStringList).indexOf(first_wavelength);
									last_wavenumber = Arrays.asList(waveStringList).indexOf(last_wavelength);
									int integralRange = Math.abs(reconstruction_last_wavenumber-reconstruction_first_wavenumber)+1;

									/*for(int i=0; i<num_wavelengths; i++){
										if(readfirstdata(wavelengthFile,i).compareTo(first_wavelength) == 0){first_wavenumber = i;}
										if(readfirstdata(wavelengthFile,i).compareTo(last_wavelength) == 0){last_wavenumber = i;}
										if(readfirstdata(wavelengthFile,i).compareTo( reconstruction_first_wavelength) == 0){reconstruction_first_wavenumber = i;}
										if(readfirstdata(wavelengthFile,i).compareTo( reconstruction_last_wavelength) == 0){reconstruction_last_wavenumber = i;}
									}*/
										if(reconstruction_last_wavenumber<reconstruction_first_wavenumber){//swaps wavenumbers if you scrrewed up//
										int x = reconstruction_last_wavenumber;
										reconstruction_last_wavenumber = reconstruction_first_wavenumber;
										reconstruction_first_wavenumber = x;
										String s = reconstruction_last_wavelength;
										reconstruction_last_wavelength = reconstruction_first_wavelength;
										reconstruction_first_wavelength = s;
									}
									
									/*
									first_dataFileName = read_dataDir+rootName+"_"+first_wavelength+"cm-1"+".dpt";
									first_dataFile = new File(first_dataFileName);
									second_dataFileName = read_dataDir+rootName+"_"+last_wavelength+"cm-1"+".dpt";
									second_dataFile = new File(second_dataFileName);
									data1 = parse_xrf(first_dataFile);
									data2 = parse_xrf(second_dataFile);
									data1_string = splitPattern.split(data1);
									data2_string = splitPattern.split(data2);
									*/
									
									int y = last_wavenumber - first_wavenumber;
									String areabaselineFileN = "area "+reconstruction_first_wavelength+"-"+reconstruction_last_wavelength+" with baseline "+first_wavelength+"-"+last_wavelength;
									for(int v=reconstruction_first_wavenumber; v<=reconstruction_last_wavenumber; v++ ){
										/*
										String s3 = readfirstdata(wavelengthFile, v);
										third_dataFileName =  read_dataDir+rootName+"_"+s3+"cm-1.dpt";
										third_dataFile = new File(third_dataFileName);
										data3 =  parse_xrf(third_dataFile);
										data3_string = splitPattern.split(data3);
										*/
										int x = v/y;
										for(int j=start_angle;j<=end_angle; j++){
											ImagePlus WaveOne;
											ImagePlus WaveTwo;
											float[] PixelArrayOne;
											float[] PixelArrayTwo;
											WaveOne = getImageDataFromH5(testFile, j, first_wavelength_choice.getSelectedItem(), 0, 0, xwidth, yheight);
											ImageProcessor WaveOnePR = WaveOne.getProcessor();
											PixelArrayOne = (float[]) WaveOnePR.getPixels();
											WaveOne.close();	
											WaveTwo = getImageDataFromH5(testFile, j, last_wavelength_choice.getSelectedItem(), 0, 0, xwidth, yheight);
											ImageProcessor WaveTwoPR = WaveTwo.getProcessor();
											PixelArrayTwo = (float[]) WaveTwoPR.getPixels();
											WaveTwo.close();	
										
											ImagePlus ips = getImageDataFromH5(testFile, j, waveStringList[v], 0, 0, xwidth, yheight);
											ImageProcessor ipr = ips.getProcessor();
											float[] PeakPixelArray = (float[]) ipr.getPixels();
											ips.close();	
											
											/*
											data1_line_string = splitPatterncomma.split((data1_string[j]));
											data2_line_string = splitPatterncomma.split((data2_string[j]));
											data3_line_string = splitPatterncomma.split((data3_string[j]));
											*/
											String st = j+","+"area with baseline"+",";	
											for (int h=0; h<area-1; h++){
												Float f1 = PixelArrayOne[h];
												Float f2 = PixelArrayTwo[h];
												Float f3 = PeakPixelArray[h];
												if(v == reconstruction_last_wavenumber){
													ftmp[h][j-start_angle] = ((f3 - ((f2-f1)*x)) + ftmp[h][j-start_angle])/integralRange;
													if(ftmp[h][j-start_angle]>6f){ftmp[h][j-start_angle]=6f;}
													else if(ftmp[h][j-start_angle]<-6f){ftmp[h][j-start_angle]=-6f;}
													st = st + ftmp[h][j-start_angle] + ",";	
												}
												else{ftmp[h][j-start_angle] = (f3 - ((f2-f1)*x)) + ftmp[h][j-start_angle];}
											}
											IJ.showProgress(j,end_angle);
										}
									}
									onewavenumberreconstruction(reconstruction_first_wavenumber,corrDir,read_dataDir,rootName,ftmp,"_trans_%04d.fld",xwidth,yheight,cropdims,submitdims,finaldims,orbit, start_angle, end_angle,nangles,cor,cor_step,steps,first_slice,areabaselineFileN);
									close_h5(testFile);
								}
							}
						}
						/*
						 * If "Reconstruction of all wavelengths" was checked, prompt user with dialog to confirm whether to reconstruct everything
						 */
						else if (allwavenumbers_box.getState() == true){
							GenericDialog gd10 = new NonBlockingGenericDialog("Confirmation window");
							gd10.setSize(600,700);
							gd10.centerDialog(true);
							gd10.addMessage("Reconstruction of all wavelengths.");
							gd10.addMessage("Orbit : "+orbit);
							gd10.addMessage("Angular Increment : "+increment);
							gd10.addMessage("Slice(s) : "+Slice);
							gd10.addMessage("Center of Rotation : "+cor);
							gd10.showDialog();	
							gd10.addChoice("Initial angle : ",degreeChoices, degreeChoices[0]);
							gd10.addChoice("Final angle : ",degreeChoices, degreeChoices[degreeChoices.length-1]);
							
							Vector the_strings10;
							the_strings10 = gd10.getChoices();
							first_degree_choice = (Choice) the_strings10.get(0);
							last_degree_choice = (Choice) the_strings10.get(1);
							/*
							 * If the dialog is canceled, program ends. Else, run the process.
							 */
							if (gd10.wasCanceled()){return;}
							else {
								open_h5(testFile);

								first_degree = first_degree_choice.getSelectedItem();
								last_degree = last_degree_choice.getSelectedItem();
								for (int i = 0; i < degreeChoices.length; i++){
									if (first_degree == degreeChoices[i]){
										start_angle = i;
									}
									if (last_degree == degreeChoices[i]){
										end_angle = i;
									}
								}
								if ((start_angle + end_angle+1)%2 != 0){
									IJ.log("Total angle dimensions are not even, Aspire reconstruction accepts even angle inputs only");
									if (start_angle == nangles - 2){
										IJ.log("Start angle has been adjusted");
										start_angle -= 1;
									}
									else {
										IJ.log("Last angle has been adjusted");
										end_angle -= 1;
									}
								}
								
								setupDims(yheight, first_slice, last_slice, start_angle, end_angle);

								String corFileName = read_dataDir +rootName+"_center_of_rotation.dpt";
								write(corFileName,cor+"");
								allwavenumbersreconstruction(testFile, corrDir,read_dataDir,rootName,"_trans_%04d.fld",xwidth,yheight,cropdims,submitdims,finaldims,orbit, start_angle, end_angle,nangles,cor,first_slice);
								IJ.log("End of the reconstruction program.");
								close_h5(testFile);
							}
						}
						else {return;}
					}
				}
				else {
					return;
				}
			}
		}
	}
     
/*	public String parse_xrf(File xrfFile){
	StringBuilder contents = new StringBuilder();
	try {
		BufferedReader input =  new BufferedReader(new FileReader(xrfFile));
		try {
			String line = null; //not declared within while loop
			while (( line = input.readLine()) != null){
				contents.append(line);
				contents.append(System.getProperty("line.separator"));
			}
		}finally {
			input.close();
		}
	}catch (IOException ex){
			ex.printStackTrace();
	}
	String xrfContents = contents.toString();
	return xrfContents;
	}
*/
/**
     * Read the first data of a given line.
     * @param file name.
     * @param int line number
     * @return the first data
*/
/*	public String readfirstdata(String FileName, int linenumber){	
		StringBuilder contents = new StringBuilder();
		Pattern splitPattern = Pattern.compile(System.getProperty("line.separator"));
		Pattern splitPatterncomma = Pattern.compile(",");
		try{
			BufferedReader input =  new BufferedReader(new FileReader(FileName));
			try {
				String line = null;
				while (( line = input.readLine()) != null){
					contents.append(line);
					contents.append(System.getProperty("line.separator"));
				}
			}finally {
				input.close();
			}
		}catch (IOException ex){
			ex.printStackTrace();
		}
		String Contents = contents.toString();
		String[] data_string = splitPattern.split(Contents);
		String[] line_string = splitPatterncomma.split((data_string[linenumber]));
		return line_string[0];
	}
*/
/**
     * @param file name.
     * @return number of lines
*//*
	public int numberoflines(String FileName){
	int numline = 0;
	try {
		BufferedReader input =  new BufferedReader(new FileReader(FileName));
		try {
			String line = null; 
			while (( line = input.readLine()) != null){numline ++;}
		}finally {
			input.close();
		}
	}catch (IOException ex){
		ex.printStackTrace();
	}
	return numline;
}*/
	
	/*
	 * Initialize image dimensions to use for Aspire reconstruction based on dataset
	 */	
	public void setupDims(int yheight, int first_slice, int last_slice, int first_angle_slice, int last_angle_slice){
		cropdims[0] = yheight;
		cropdims[1] = last_slice-first_slice+1;
		cropdims[2] = last_angle_slice-first_angle_slice+1;

		IJ.log("cropdims[0] = "+cropdims[0]);
		IJ.log("cropdims[1] = "+cropdims[1]);
		IJ.log("cropdims[2] = "+cropdims[2]);
		if (cropdims[2]%2>0){
			cropdims[2]=cropdims[2]-1;
		}

		submitdims[0] = cropdims[0];
		submitdims[1] = last_angle_slice-first_angle_slice+1;
		submitdims[2] = cropdims[1];
			
		IJ.log("submitdims[0] = "+submitdims[0]);
		IJ.log("submitdims[1] = "+submitdims[1]);
		IJ.log("submitdims[2] = "+submitdims[2]);
		
		finaldims[0] = submitdims[0];
		finaldims[1] = submitdims[0];
		finaldims[2] = submitdims[2];
		
		IJ.log("finaldims[0] = "+finaldims[0]);
		IJ.log("finaldims[1] = "+finaldims[1]);
		IJ.log("finaldims[2] = "+finaldims[2]);
		
	}

/*
     * Add a line to the end of file. The line is given by a string parameter.
     * @param file name.
     * @param string.
*/
	public void addline(String FileName,String str){
		try {
		BufferedReader output =  new BufferedReader(new FileReader(FileName));
			try {
				BufferedWriter bufferwriter = new BufferedWriter(new FileWriter(FileName, true)); 
				bufferwriter.write(str); 
				bufferwriter.newLine();
				bufferwriter.close();
			}
			finally{
				output.close();
			}
		}
		catch (IOException ex){
			ex.printStackTrace();
		}
	}	

/**
     * write a string parameter to a file.
     * @param file adress.
     * @param string.
*/
	public void write (String Fileadress, String str){
		try {
			FileWriter fw = new FileWriter(Fileadress);
			BufferedWriter output = new BufferedWriter(fw);
			try{
				output.write(str);
				output.flush();
			}finally {
				output.close();
			}
		}catch(IOException ioe){
			System.out.print("Erreur : ");
			ioe.printStackTrace();
		}
	}

	/*
	 * Saves .fld format that Aspire reconstruction uses to reconstruct dataset
	 */
	static public void saveFLD(ImagePlus imp, String path) {
		File file = new File(path);
		DataOutputStream dos = null;
		try {
			dos = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(file)));
			// read data:
			FileInfo fi = imp.getFileInfo();
			// HEADER: ... read all header tags and metadata
			int width = (int)(imp.getWidth());
			int height = (int)(imp.getHeight());
			int slices = (int)(imp.getNSlices());                           
			String header;
			if (slices > 1){header = "# AVS field file\nndim=3\ndim1="+width+"\ndim2="+height+"\ndim3="+slices+"\nnspace=3\nveclen=1\ndata=xdr_float\nfield=uniform\n\f\f";}
			else{header = "# AVS field file\nndim=2\ndim1="+width+"\ndim2="+height+"\nnspace=2\nveclen=1\ndata=xdr_float\nfield=uniform\n\f\f";}
			dos.writeBytes(header);
		                       
			for (int i=1; i<slices+1; i++) {
				imp.setSlice(i);
				float[] pixels = (float[])imp.getProcessor().getPixels();
				for (int j = 0; j < width*height; j++) {
					dos.writeFloat(pixels[j]);
				}
			}
			dos.flush();
			dos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/*
	 * Calls Aspire reconstruction software to reconstruct to corrDirectory using data from read_dataDirectory
	 */
	public String aspire_reconstruction(String corrDirectory,String read_dataDirectory,String the_file_name,String file_ending,int file_number, int[] aspire_dims, float orbit, float cor, String TitleImage){
		String dsc_file = "";
		String addon = "_trans_fbp_%04d_cor"+cor;
		String addon2 = "_trans_trpl_";
		try{
			dsc_file = corrDirectory+"tomo.dsc";
			FileWriter outFile = new FileWriter(dsc_file);
			PrintWriter out = new PrintWriter(outFile);
			out.println("system 2");
			out.println("nx "+aspire_dims[0]);
			out.println("ny "+aspire_dims[0]);
			out.println("nb "+aspire_dims[0]);
			out.println("na "+aspire_dims[1]);
			out.println("support ellipse 0 0 "+(aspire_dims[0]/2-2)+" "+(aspire_dims[0]/2-2));
			out.println("offset_even "+cor);
			out.println("offset_odd "+cor);
			out.println("orbit "+orbit);
			out.println("orbit_start 0");
			out.println("scale 0");
			out.println("strip_width 1");
			out.close();	
		}catch (IOException e){
			e.printStackTrace();
			}
	
		//C:\\Documents and Settings\\Michael Martin\\
		String[] args = {"C:\\aspire\\input.exe","fbp2t","dsc",String.format(corrDirectory+the_file_name+"_trans_fbp_%04d_cor"+cor+".fld",file_number),"-",String.format(corrDirectory+the_file_name+file_ending,file_number),"-","1","-","0",dsc_file};
		if(IJ.isWindows() == false){args[0] = "/dula/software/src/aspire/i";}
		String line;
		try{
			ProcessBuilder procbuild1 = new ProcessBuilder(args);
			procbuild1.redirectErrorStream(true);
			Process proc1 =procbuild1.start();
			BufferedReader buffrdr1 = new BufferedReader(new InputStreamReader(proc1.getInputStream()));
			while ((line = buffrdr1.readLine()) != null){
				IJ.log("Aspire: "+line);			
			}
			IJ.log("Aspire recon finished!");

		}catch (Throwable t){t.printStackTrace();}
		
		String output_name = String.format(corrDirectory+the_file_name+"_trans_fbp_%04d_cor"+cor+"_"+TitleImage+".raw",file_number);
		String[] args2 = {"C:\\aspire\\output.exe","conv",output_name,String.format(corrDirectory+the_file_name+"_trans_fbp_%04d_cor"+cor+".fld",file_number),"raw"};
		if(IJ.isWindows() == false){args2[0] = "/dula/software/src/aspire/op";}   	
		try{
			ProcessBuilder procbuild2 = new ProcessBuilder(args2);
			procbuild2.redirectErrorStream(true);
			Process proc2 =procbuild2.start();
			BufferedReader buffrdr2 = new BufferedReader(new InputStreamReader(proc2.getInputStream()));
			while ((line = buffrdr2.readLine()) != null){
				IJ.log("Aspire: "+line);
			}
			IJ.log("Aspire recon finished!");

		}catch (Throwable t){
			t.printStackTrace();
		}
		return output_name;
	}

	/*
	 * writes Raw image
	 */
	public ImagePlus open_raw(String filename,int[] dims){
		FileInfo fi = new FileInfo();
		fi.fileFormat = fi.RAW;
		fi.fileName = filename.substring(filename.lastIndexOf(Prefs.separator)+1,filename.length());
		fi.directory = filename.substring(0,filename.lastIndexOf(Prefs.separator));
		fi.width = dims[0];
		fi.height = dims[1];
		fi.offset = 0;
		fi.nImages = dims[2];
		fi.gapBetweenImages = 0;
		fi.intelByteOrder = false;
		fi.whiteIsZero = false;
		fi.fileType = FileInfo.GRAY32_FLOAT;
		            
		FileOpener fo = new FileOpener(fi);
		ImagePlus fbp = fo.open(false);
		return fbp;	
	}

	/*
	 * deletes a file if it exists
	 */
	public void delete_if_exist(String filename){
		File alreadyexist = new File(filename);
		if (alreadyexist.exists() == true){
			alreadyexist.delete();
		}
	}

/**
     * Make the reconstruction for one wavelength image from data directly from the h5 file.
*/
	public void onewavenumberreconstruction(int wavenum, H5File testFile, Choice first_wavelength_choice, String corrDirectory,String read_dataDirectory,String root_name,String file_ending,int xwidth, int yheight,int[] reconstruct_cropdims,int[] reconstruct_submitdims, int[] reconstruct_finaldims, float orbit, int start_angle, int end_angle, int nangles, float cor_low, float cor_step, int steps, int first_slice,String TitleImage){
		ImageStack wavenumberStack = new ImageStack(reconstruct_cropdims[0],reconstruct_cropdims[1]);
		/*String dataFileName = read_dataDirectory+root_name+"_"+TitleImage+".dpt";
		File dataFile = new File(dataFileName);
		String angleContents = parse_xrf(dataFile);
		Pattern splitPattern = Pattern.compile(System.getProperty("line.separator"));
		Pattern splitPatterncomma = Pattern.compile(",");
		String[] angle_data_string = splitPattern.split(angleContents);*/
		IJ.log("Reading data from "+testFile);

	
		int reconstruct_area = xwidth*yheight;
		for (int d=start_angle;d<=end_angle;d++){

			ImagePlus initWave = getImageDataFromH5(testFile, d, first_wavelength_choice.getSelectedItem(), 0, 0, xwidth, yheight);
			ImageProcessor initWavePR = initWave.getProcessor();
			float[] angle_pixels_float = (float[]) initWavePR.getPixels();
			initWave.close();
			//String[] angle_line_string = splitPatterncomma.split((angle_data_string[d]));
			/*for (int x=0;x<reconstruct_area-1;x++){
				Next two lines replace above line to make absorbance data into transmission.
				angle_pixels_float[x] = (float) (Float.valueOf(angle_line_string[x+2].trim()).floatValue());
				Math.exp(-Float.valueOf(angle_line_string[x+2].trim()).floatValue());
			}*/
			//initWavePR.setPixels(angle_pixels_float);
			initWavePR.rotate(90);
			initWavePR.setRoi(0, first_slice, reconstruct_cropdims[0], reconstruct_cropdims[1]);
			ImageProcessor angleprocessor_crop = initWavePR.crop();
			wavenumberStack.addSlice(""+d,angleprocessor_crop);
			IJ.showProgress(d, end_angle);
		}
		ImagePlus transImage = new ImagePlus("stack",wavenumberStack);
		IJ.saveAs(transImage,"Tiff",String.format(corrDirectory+root_name+"_trans_stack_%04d"+"_"+TitleImage+".tif",wavenum));
		Slicer the_slicer = new Slicer();
		ImagePlus resliced_trans = the_slicer.reslice(transImage);
		saveFLD(resliced_trans,String.format(corrDirectory+root_name+"_trans_%04d.fld",wavenum));
		IJ.saveAs(resliced_trans,"Tiff",String.format(corrDirectory+root_name+"_trans_%04d"+"_"+TitleImage+".tif",wavenum));
		for (int i=0;i<steps;i++){
			float cor = cor_low+i*cor_step;
			delete_if_exist(corrDirectory+"tomo.dsc");
			delete_if_exist(String.format(corrDirectory+root_name+"_trans_fbp_%04d_cor"+cor+".fld",wavenum));
			delete_if_exist(String.format(corrDirectory+root_name+"_trans_fbp_%04d_cor"+cor+"_"+TitleImage+".raw",wavenum));

			delete_if_exist(corrDirectory+"tomo.wtf");
			String recon_output = " ";
			IJ.log("Starting reconstruction.");
			recon_output = aspire_reconstruction(corrDirectory,read_dataDirectory,root_name,"_trans_%04d.fld",wavenum, reconstruct_submitdims, orbit, cor,TitleImage);
			ImagePlus fbp = open_raw(recon_output,reconstruct_finaldims);
			fbp.show();
			IJ.showProgress(i, steps);
		}
	}

/**
     * Make the reconstruction for one wavelength image from float array data calculated by subtraction/integral/area/etc procresses.
*/
	public void onewavenumberreconstruction(int wavenum,String corrDirectory,String read_dataDirectory,String the_file_name,float[][] data, String file_ending,int xwidth, int yheight,int[] reconstruct_cropdims,int[] reconstruct_submitdims, int[] reconstruct_finaldims, float orbit, int start_angle, int end_angle, int nangles, float cor_low, float cor_step, int steps, int first_slice,String TitleImage){
		ImageStack wavenumberStack = new ImageStack(reconstruct_cropdims[0],reconstruct_cropdims[1]);
		int reconstruct_area = xwidth*yheight;
		IJ.log("Reading data");

		for (int d=start_angle;d<=end_angle;d++){
			FloatProcessor angleprocessor = new FloatProcessor(xwidth,yheight);
			float[] angle_pixels_float = new float[reconstruct_area];
			for (int x=0;x<reconstruct_area-1;x++){
				// Next two lines replace above line to make absorbance data into transmission.
				angle_pixels_float[x] = (float)
				/*Math.exp(-Float.valueOf(data[x][d]).floatValue());*/
				(Float.valueOf(data[x][d-start_angle]).floatValue());
			}
			angleprocessor.setPixels(angle_pixels_float);
			angleprocessor.rotate(90);
			angleprocessor.setRoi(0, first_slice, reconstruct_cropdims[0], reconstruct_cropdims[1]);
			ImageProcessor angleprocessor_crop = angleprocessor.crop();
			wavenumberStack.addSlice(""+d,angleprocessor_crop);
			IJ.showProgress(d, end_angle);
		}
		ImagePlus transImage = new ImagePlus("stack",wavenumberStack);
		IJ.saveAs(transImage,"Tiff",String.format(corrDirectory+the_file_name+"_trans_stack_%04d"+"_"+TitleImage+".tif",wavenum));
		Slicer the_slicer = new Slicer();
		ImagePlus resliced_trans = the_slicer.reslice(transImage);
		saveFLD(resliced_trans,String.format(corrDirectory+the_file_name+"_trans_%04d.fld",wavenum));
		IJ.saveAs(resliced_trans,"Tiff",String.format(corrDirectory+the_file_name+"_trans_%04d"+"_"+TitleImage+".tif",wavenum));
		for (int i=0;i<steps;i++){
			float cor = cor_low+i*cor_step;
			delete_if_exist(corrDirectory+"tomo.dsc");
			delete_if_exist(String.format(corrDirectory+the_file_name+"_trans_fbp_%04d_cor"+cor+".fld",wavenum));
			delete_if_exist(String.format(corrDirectory+the_file_name+"_trans_fbp_%04d_cor"+cor+"_"+TitleImage+".raw",wavenum));
			delete_if_exist(corrDirectory+"tomo.wtf");
			String recon_output = " ";
			IJ.log("Starting reconstruction.");
			recon_output = aspire_reconstruction(corrDirectory,read_dataDirectory,the_file_name,"_trans_%04d.fld",wavenum, reconstruct_submitdims, orbit, cor,TitleImage);
			ImagePlus fbp = open_raw(recon_output,reconstruct_finaldims);
			fbp.show();
			IJ.showProgress(i, steps);
		}
	}

/**
     * Make the reconstruction for all wavelengths.
*/
	public void allwavenumbersreconstruction(H5File testFile, String corrDirectory,String read_dataDirectory,String the_file_name,String file_ending, int xwidth, int yheight,int[] reconstruct_cropdims,int[] reconstruct_submitdims, int[] reconstruct_finaldims, float orbit, int start_angle, int end_angle, int nangles, float cor ,int first_slice){
		String wavelengthFileName = read_dataDirectory+the_file_name+"_wavelength.dpt";
		for (int a=0;a<bands;a++){
			ImageStack wavenumberStack = new ImageStack(reconstruct_cropdims[0],reconstruct_cropdims[1]);
			String dataFileName = read_dataDirectory+the_file_name+"_"+waveStringList[a]+"cm-1.dpt";
			IJ.log("Reading "+dataFileName);
			/*
			File dataFile = new File(dataFileName);
			String angleContents = parse_xrf(dataFile);
			Pattern splitPattern = Pattern.compile(System.getProperty("line.separator"));
			Pattern splitPatterncomma = Pattern.compile(",");
			String[] angle_data_string = splitPattern.split(angleContents);*/
			
			int reconstruct_area = xwidth*yheight;
			for (int d=start_angle;d<=end_angle;d++){
				ImagePlus Wave = getImageDataFromH5(testFile, d, waveStringList[a], 0, 0, xwidth, yheight);
				ImageProcessor angleprocessor = Wave.getProcessor();
				float[] angle_pixels_float = (float[]) angleprocessor.getPixels();
				Wave.close();	
				//String[] angle_line_string = splitPatterncomma.split((angle_data_string[d]));
				/*for (int x=0;x<reconstruct_area-1;x++){
					// Next two lines replace above line to make absorbance data into transmission.
					angle_pixels_float[x] = (float)
					//Math.exp(-Float.valueOf(angle_line_string[x+2].trim()).floatValue());
					(Float.valueOf(angle_line_string[x+2].trim()).floatValue());
				}*/
				angleprocessor.rotate(90);
				angleprocessor.setRoi(0, first_slice, reconstruct_cropdims[0], reconstruct_cropdims[1]);
				ImageProcessor angleprocessor_crop = angleprocessor.crop();
				wavenumberStack.addSlice(""+d,angleprocessor_crop);
				IJ.showProgress(d, end_angle);
			}
			String TitleImage = waveStringList[a] + "cm-1";
			ImagePlus transImage = new ImagePlus("stack",wavenumberStack);
			IJ.saveAs(transImage,"Tiff",String.format(corrDirectory+the_file_name+"_trans_stack_%04d"+"_"+TitleImage+".tif",a));
			Slicer the_slicer = new Slicer();
			ImagePlus resliced_trans = the_slicer.reslice(transImage);
			saveFLD(resliced_trans,String.format(corrDirectory+the_file_name+"_trans_%04d.fld",a));
			IJ.saveAs(resliced_trans,"Tiff",String.format(corrDirectory+the_file_name+"_trans_%04d"+"_"+TitleImage+".tif",a));
			delete_if_exist(corrDirectory+"tomo.dsc");
			delete_if_exist(String.format(corrDirectory+the_file_name+"_trans_fbp_%04d_cor"+cor+".fld",a));
			delete_if_exist(String.format(corrDirectory+the_file_name+"_trans_fbp_%04d_cor"+cor+"_"+TitleImage+".raw",a));
			delete_if_exist(corrDirectory+"tomo.wtf");
			String recon_output = "";
			recon_output = aspire_reconstruction(corrDirectory,read_dataDirectory,the_file_name,"_trans_%04d.fld",a, reconstruct_submitdims, orbit, cor,TitleImage);
			ImagePlus fbp = open_raw(recon_output,reconstruct_finaldims);
		}
	}
}

/*
 * Deprecated, used in read_packaging.java and ir_tomo.java
 */
class dptfilter implements FilenameFilter 
{
	public boolean accept(File dir, String name){return (name.endsWith(".DPT") || name.endsWith(".dpt"));}
}





	

