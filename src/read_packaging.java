import java.io.*;
import java.awt.*;
import java.util.*;
import ij.*;
import ij.process.*;
import ij.gui.*;
import java.awt.*;
import ij.plugin.*;
import ij.macro.*;
import ij.io.*;
import javax.swing.filechooser.*;
import javax.swing.*;
import javax.swing.SwingUtilities;
import java.io.FilenameFilter;
import java.io.File;
import java.util.regex.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.Checkbox;
import java.awt.TextField;
import ij.measure.*;
import static ij.plugin.Slicer.*;

public class read_packaging implements PlugIn 
{
public void run(String arg) 
{
	String start_path = "";
	if(IJ.isWindows() == true)
	{
       		// start_path = "C:\\Documents and Settings\\Michael Martin\\My Documents\\FTIR Tomography\\";
       		start_path = "I:\\Martin\\";
	} 
	else{start_path = "/dula/users832/mam/seed/";}
	JFileChooser fc1 = new JFileChooser(start_path);
		
       	String original_file = "";
       	int returnVal = fc1.showOpenDialog(null);
 	if (returnVal == JFileChooser.APPROVE_OPTION) {original_file = fc1.getSelectedFile().getPath();}
        else {return;}
        if (original_file==null) {return;}
	else{}

	File testFile = new File(original_file);
	String testContents = parse_xrf(testFile);
	int line_separator_length = System.getProperty("line.separator").length();
	Pattern splitPattern = Pattern.compile(System.getProperty("line.separator"));
	String[] test_data_string = splitPattern.split(testContents);
	int[] dims = new int[3];
	dims[2] = test_data_string.length;
	Pattern splitPatterncomma = Pattern.compile(",");
	String[] test_line_string = splitPatterncomma.split(test_data_string[0]);
	dims[0] = (int) Math.floor((double) Math.sqrt((double)(test_line_string.length - 1.0)));
	dims[1] = (int) Math.floor(((double)test_line_string.length / (double)dims[0]));
	int area = dims[0]*dims[1];

	ImageStack testStack = new ImageStack(dims[0],dims[0]);
	for (int k=0;k<dims[2];k++)
	{
		test_line_string = splitPatterncomma.split(test_data_string[k]);
		FloatProcessor testprocessor = new FloatProcessor(dims[0],dims[1]);
		float[] test_pixels_float = new float[area];
		for (int x=0;x<area;x++){test_pixels_float[x] = Float.valueOf(test_line_string[x+1].trim()).floatValue();}
		testprocessor.setPixels(test_pixels_float);
		ImageProcessor tempProcessor = testprocessor.duplicate();
		testStack.addSlice("cm-1: "+test_line_string[0],tempProcessor);
	}
	ImagePlus testImage = new ImagePlus("test",testStack);
	testImage.show();	

	String root_file_name = original_file.substring(original_file.lastIndexOf("\\")+1,original_file.lastIndexOf("_"));
	String directory = original_file.substring(0,original_file.lastIndexOf("\\"));
	if (directory == ""){return;}
	else{directory = directory+Prefs.separator;}

	String read_dataDir = directory+"read_data";
	read_dataDir = read_dataDir+Prefs.separator;
	
	File outputfile1 = new File(read_dataDir);
	if (!outputfile1.exists()){outputfile1.mkdir();}
	if (!outputfile1.exists()){IJ.showMessage("Couldn't create requested output directory.");return;}
	
	//create a file list filter to automatically detect parameters
	File sourcedirectory = new File(directory);
	FilenameFilter filter_dpt1 = new dptfilter();
	String[] filelist_dpt = sourcedirectory.list(filter_dpt1);
	int nangles = filelist_dpt.length;
	String sample_file_format = root_file_name+"_%04d.DPT";

	//new dialog with recon parameters
	GenericDialog gd2 = new NonBlockingGenericDialog("Read Packaging");
	gd2.setSize(600,700);
	gd2.centerDialog(true);
	gd2.addHelp("https://sites.google.com/a/lbl.gov/microct/user-resources/reconstruction-plugin-instructions");
	gd2.addStringField("Input File Pattern (%04d=four-digit integers with zero-padding)",sample_file_format,50);
	float orbit = 180;
	gd2.addNumericField("Orbit (total angle)",orbit,3);
	gd2.addNumericField("Angular Increment",(int)orbit/(nangles-1),3);
	
	//got some ideas for this from http://dev.loci.wisc.edu/trac/software/browser/trunk/components/loci-plugins/src/loci/plugins/importer/ImporterDialog.java?rev=6039#L403
	TextField file_pattern_field;
	TextField orbit_field;
	TextField increment_field;

	Vector the_numbers2;
	Vector the_strings2;
	the_numbers2 = gd2.getNumericFields();
	orbit_field = (TextField) the_numbers2.get(0);
	increment_field = (TextField) the_numbers2.get(1);
	the_strings2 = gd2.getStringFields();
	file_pattern_field = (TextField) the_strings2.get(0);
 	gd2.showDialog();
	if (gd2.wasCanceled()){return;}
	else 
	{
		float increment = Float.parseFloat(increment_field.getText());
		orbit = Float.parseFloat(orbit_field.getText());
		nangles=(int)(((int)orbit/increment)+1);
		
		GenericDialog gd3 = new NonBlockingGenericDialog("Confirmation window");
		gd3.setSize(600,700);
		gd3.centerDialog(true);
		gd3.addMessage("Readpackaging Program :");
		gd3.addMessage("Orbit : "+orbit);
		gd3.addMessage("Increment : "+increment);
		gd3.addMessage("Number of files : "+(nangles+1));
		gd3.showDialog();
		if (gd3.wasCanceled()){return;}
		else 
		{
			// Creation of file to memorize angle value and increment value
			String angledataFileName = read_dataDir+root_file_name+"_angle.dpt";
			write(angledataFileName,increment+"\n");
			addline(angledataFileName,orbit+"");
		
			// Creation of files for each wavelength and writing data in correct files and Creation of file to memorize wavelengths.
			for (int c=0;c<=nangles;c++)
			{
				String angleFileName = String.format(directory+sample_file_format,c);
				IJ.log("Reading "+angleFileName);
				File angleFile = new File(angleFileName);
				try {
			     		BufferedReader input =  new BufferedReader(new FileReader(angleFileName));
			     		try {
						String line = null;
						if(c == 0)
						{
							int num_lines = 0;
							String wavelengthFileName = read_dataDir+root_file_name+"_wavelength.dpt";
					 		write(wavelengthFileName,"");
					 		while (( line = input.readLine()) != null)
					 		{
					 			num_lines++;
					 			String[] test_data = splitPattern.split(line);
					 			Pattern SplitPatterncomma = Pattern.compile(",");
								String[] test = SplitPatterncomma.split(test_data[0]);
					 			String FileName = read_dataDir+root_file_name+"_"+test[0]+"cm-1.dpt";
					 			addline(wavelengthFileName, test[0]+","+ num_lines);
					 			write(FileName, c+","+line+"\n");	
					 		}
						}
						else
						{
							while (( line = input.readLine()) != null)
					 		{
					 			String[] test_data = splitPattern.split(line);
					 			Pattern SplitPatterncomma = Pattern.compile(",");
								String[] test = SplitPatterncomma.split(test_data[0]);
					 			String FileName = read_dataDir+root_file_name+"_"+test[0]+"cm-1.dpt";
					 			addline(FileName, c+","+line);
					 		}
						}
					}finally {input.close();}
				}catch (IOException ex){ex.printStackTrace();}
			}
			IJ.log("End of the read packaging program.");
		}
	}
}

public String parse_xrf(File xrfFile)
{
	StringBuilder contents = new StringBuilder();
		try {
	     		BufferedReader input =  new BufferedReader(new FileReader(xrfFile));
	     		try {
				String line = null; 
		 		while (( line = input.readLine()) != null)
		 		{
					contents.append(line);
					contents.append(System.getProperty("line.separator"));
				}
			}finally { input.close();}
		}catch (IOException ex){ex.printStackTrace();}
		String xrfContents = contents.toString();
		return xrfContents;
}

/**
     * write a string parameter to a file.
     * @param file adress.
     * @param string.
*/
public void write (String Fileadress, String str)
{
	try{
		FileWriter fw = new FileWriter(Fileadress);
		BufferedWriter output = new BufferedWriter(fw);
		try{
			output.write(str);
			output.flush();
		}finally { output.close();}
	}catch(IOException ioe){
	System.out.print("Erreur : ");
	ioe.printStackTrace();
	}
}

/**
     * Add a line to the end of file. The line is given by a string parameter.
     * @param file name.
     * @param string.
*/
public void addline(String FileName,String str)
{
	try{
		BufferedReader output =  new BufferedReader(new FileReader(FileName));
		try{
			BufferedWriter bufferwriter = new BufferedWriter(new FileWriter(FileName, true)); 
			bufferwriter.write(str); 
			bufferwriter.newLine();
			bufferwriter.close();
		}finally{output.close();}
	}catch (IOException ex){ex.printStackTrace();}
}

}

/*class dptfilter implements FilenameFilter 
{
	public boolean accept(File dir, String name) {return (name.endsWith(".DPT") || name.endsWith(".dpt"));}
}
*/