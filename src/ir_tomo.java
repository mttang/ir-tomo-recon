import java.io.*;
import java.awt.*;
import java.util.*;
import java.util.List;

import ij.*;
import ij.process.*;
import ij.gui.*;

import java.awt.*;

import ij.plugin.*;
import ij.macro.*;
import ij.io.*;

import javax.swing.filechooser.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.*;

import java.io.FilenameFilter;
import java.io.File;
import java.util.Map.Entry;
import java.util.regex.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.Checkbox;
import java.awt.TextField;

import ij.measure.*;
import static ij.plugin.Slicer.*;
import ncsa.hdf.hdf5lib.exceptions.HDF5Exception;
import ncsa.hdf.object.Attribute;
import ncsa.hdf.object.Dataset;
import ncsa.hdf.object.Datatype;
import ncsa.hdf.object.FileFormat;
import ncsa.hdf.object.Group; // the common object package
import ncsa.hdf.object.HObject;
import ncsa.hdf.object.h5.H5File;// the HDF5 implementation


public class ir_tomo implements PlugIn {
	GenericDialog gd2 = new NonBlockingGenericDialog("Reconstruction");
	String rootName;
	String directory;
	String pluginPath;
	TextField file_pattern_field;
	TextField wavenumber_field;
	TextField slice_field;
	TextField cor_low_field;
	TextField cor_high_field;
	TextField cor_step_field;
	TextField orbit_field;
	TextField increment_field;
	TextField background_field;
	TextField iterations_field;
	TextField beta_field;
	Checkbox vertical_box;

	Vector the_checkboxes2;
	Vector the_numbers2;
	Vector the_strings2;
	
	String addon = "";
	String addon2 = "";
	String recon_output = "";
	int num_images = 1;
	int steps;
	float cor;
	float cor_low;
	float cor_high;
	float cor_step;
	float orbit;
	float increment;
	float beta;

	int iterations;
	int bands;
	int xwidth;
	int yheight;
	int tempxwidth;
	int tempyheight;
	
	String[] waveStringList;
	List<Group> degreeList;
	float[] degreeArray;
	Map<String, Object> waveMap = new HashMap<String, Object>();
	Map<String, Object> degreeMap = new HashMap<String, Object>();

	//create a file list filter to automatically detect parameters
	File dataset;
	FilenameFilter filter_dpt1 = new dptfilter();
	String[] filelist_dpt;
	int nangles;
	String sample_file_format;
	
	ImageStack imgStack;
	ImagePlus fullStack;
	int imagesLoaded = 0;
	int firstImage;
	int lastImage;
	int imageIncrement=1; 
	
	public static void main(String[] args){
		new ir_tomo().run(null);
	}
	
	private String getTheInputName(){
		String input = "";
		JFileChooser fc = new JFileChooser(OpenDialog.getLastDirectory());
		FileNameExtensionFilter filter = new FileNameExtensionFilter("H5 File", "h5 file", "h5", "hdf5");
		fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		fc.setFileFilter(filter);
		fc.setDialogTitle("Select h5 file containing ENVI Image Data and metadata");
		fc.showDialog(null, "Select");
		input = fc.getSelectedFile().getAbsolutePath();
		OpenDialog.setLastDirectory(fc.getSelectedFile().getPath().substring(0,fc.getSelectedFile().getPath().lastIndexOf(Prefs.separator)+1));
		
		if (Menus.getPlugInsPath()==null){
			pluginPath = "C:\\";
		} else
			pluginPath = Menus.getPlugInsPath();
		if(input == null){
			input = "";
		}
		IJ.log(input);

	    return input;
	 }
	private void makeGUI(){
		gd2.setSize(600,700);
		gd2.centerDialog(true);
		gd2.addHelp("https://sites.google.com/a/lbl.gov/microct/user-resources/reconstruction-plugin-instructions");

		gd2.addStringField("Input File Pattern (%04d=four-digit integers with zero-padding)",sample_file_format,50);
		gd2.addStringField("Wavenumber(s) (single dash-separated range, or single int)",""+((int) bands/2),10);
		gd2.addStringField("Slice(s) (single dash-separated range, or single int)",""+((int) bands/2),10);
		gd2.addNumericField("Orbit",orbit,3);
		gd2.addNumericField("Angular Increment",increment,3);
		gd2.addNumericField("Iterations",0,0);
		gd2.addNumericField("Beta(regularization)",5,2);
		gd2.addCheckbox("Vertical Axis of Rotation",true);
		gd2.addNumericField("Center Of Rotation (low)",0,3);
		gd2.addNumericField("Center Of Rotation (high)",0,3);
		gd2.addNumericField("Center Of Rotation (step)",1,3);
		
		the_checkboxes2 = gd2.getCheckboxes();
		vertical_box = (Checkbox) the_checkboxes2.get(0);
		//got some ideas for this from http://dev.loci.wisc.edu/trac/software/browser/trunk/components/loci-plugins/src/loci/plugins/importer/ImporterDialog.java?rev=6039#L403

		the_numbers2 = gd2.getNumericFields();
		orbit_field = (TextField) the_numbers2.get(0);
		increment_field = (TextField) the_numbers2.get(1);
		iterations_field = (TextField) the_numbers2.get(2);
		beta_field = (TextField) the_numbers2.get(3);
		cor_low_field = (TextField) the_numbers2.get(4);
		cor_high_field = (TextField) the_numbers2.get(5);
		cor_step_field = (TextField) the_numbers2.get(6);
		the_strings2 = gd2.getStringFields();
		file_pattern_field = (TextField) the_strings2.get(0);
		wavenumber_field  = (TextField) the_strings2.get(1);
		slice_field  = (TextField) the_strings2.get(2);
	}
	
	Dataset getDataset(String imageName, List<HObject> memberList){
		Dataset dataset =null;
		for(HObject object: memberList){
			String objectName = object.getName();
			if(objectName.equals(imageName) && object instanceof Dataset){
				dataset = (Dataset) object;
				break;
			}
		}
		return dataset;
	}
	private void makeImageStack(H5File ExFile, Map waveMap, String[] waveList, int degree, int firstImage, int lastImage, int imageIncrement){
		try{
			ExFile.open();
			try{
				//read H5 file to get data from the specified "image name"
				DefaultMutableTreeNode ret = (DefaultMutableTreeNode) ExFile.getRootNode();
				Group root = (Group) ret.getUserObject();
				degreeList = root.getMemberList();
				for (int i = 0; i < degreeList.size(); i++){
					if (degreeList.get(i).toString().contains("degrees")){
						degreeMap.put(degreeList.get(i).toString().substring(0,degreeList.get(i).toString().lastIndexOf("degrees")),degreeList.get(i));
					}
				}
				
				Group group;
				if(degree == Math.round(degree)){ // if the degree list is integer
					group = (Group) degreeList.get(degreeList.indexOf(degreeMap.get(Integer.toString((int)degree))));

				} else{
					group = (Group) degreeList.get(degreeList.indexOf(degreeMap.get(Float.toString(degree))));
				}
				List<HObject> memberList = group.getMemberList();

				for (int i = 0; i < memberList.size(); i++){
					for (int j = 0; j < waveStringList.length; j++){
						if (memberList.get(i).toString().contains(waveStringList[j])){
							waveMap.put(waveStringList[j], memberList.get(i));
						}
					}
				}
				getImageStackFromH5(group, waveMap, waveList, firstImage, lastImage, imageIncrement);
				
	
				
				if(imgStack!=null){
					fullStack = new ImagePlus(degreeList.get(degreeList.indexOf(degreeMap.get(Integer.toString(degree)))).toString(), imgStack);
					fullStack.show();
				}else{
					throw new Exception("Couldn't load imageStack from H5 File");
				}
				imgStack = null;
			}catch(HDF5Exception hdf5ex){
				IJ.log(hdf5ex.toString());
				if(imagesLoaded != 0){
					IJ.log("Only able to load "+imagesLoaded+" of "+(int)((lastImage - firstImage + 1)/(imageIncrement))+" images.");
					fullStack = new ImagePlus(degreeList.get(degreeList.indexOf(degreeMap.get(Integer.toString(degree)))).toString(), imgStack);
					fullStack.show();
				}
			}
			ExFile.close();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
	}	
	private void getImageStackFromH5(Group group, Map waveMap, String[] waveList, int firstImage, int lastImage, int imageIncrement){
		try{
			List<HObject> memberList = group.getMemberList();
			Dataset dataset = null;
			ImagePlus imp = null;
			IJ.log(waveMap.get(waveList[1]).toString());

			for(int i = firstImage; i < lastImage; i+= imageIncrement){
				HObject object = memberList.get(memberList.indexOf(waveMap.get(waveList[i])));
				if(object instanceof Dataset){
					dataset = (Dataset) object;
					dataset.init();
					long[] dimensions = dataset.getDims(); // dimensions[1] = Height, dimensions[2] = Width
					long[] selected = dataset.getSelectedDims(); //the size in each dimension to select
			        		     
			        if(dimensions.length > 3){
						throw new RuntimeException("Unsure how to handle dataset with number of dimensions = "+dimensions.length);
					}
			        
			        // set the selection arrays
			        
					selected[0] = dimensions[0];
					selected[1] = dimensions[1];
					
					Object data = dataset.getData();
					if(data instanceof byte[]){
						byte[] pixels = (byte[])data;
						imp = new ImagePlus(dataset.getFullName(), new ByteProcessor((short)selected[1], (short)selected[0], pixels));
					}else if(data instanceof short[]){
						short[] pixels = (short[])data;
						imp = new ImagePlus(dataset.getFullName(), new ShortProcessor((short)selected[1], (short)selected[0], pixels, null));
					}else if(data instanceof float[]){
						float[] pixels = (float[])data;
						imp = new ImagePlus(dataset.getFullName(), new FloatProcessor((short)selected[1], (short)selected[0], pixels, null));
					}else{
						int[] pixels = (int[])data;
						imp = new ImagePlus(dataset.getFullName(), new ColorProcessor((short)selected[1], (short)selected[0], pixels));
					}
					dataset.clear();
					if(imgStack == null){
						imgStack = new ImageStack((short)dimensions[1], (short)dimensions[0]);
					}
					imgStack.addSlice(imp.getProcessor());
					imagesLoaded++;
					imp = null;
					IJ.showProgress(imagesLoaded, (int)((lastImage - firstImage + 1)/imageIncrement));
				}
			}
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	ImagePlus getImageDataFromH5(H5File ExFile, float degree, String band, int xOffset, int yOffset, int xLength, int yLength){
		ImagePlus imp = null;
		try{
			ExFile.open();
			//read H5 file to get data from the specified "image name"
			DefaultMutableTreeNode ret = (DefaultMutableTreeNode) ExFile.getRootNode();
			Group root = (Group) ret.getUserObject();
			degreeList = root.getMemberList();
			for (int i = 0; i < degreeList.size(); i++){
				if (degreeList.get(i).toString().contains("degrees")){
					degreeMap.put(degreeList.get(i).toString().substring(0,degreeList.get(i).toString().lastIndexOf("degrees")),degreeList.get(i));
				}
			}

			Group group;
			if(degree == Math.round(degree)){ // if the degree list is integer
				group = (Group) degreeList.get(degreeList.indexOf(degreeMap.get(Integer.toString((int)degree))));

			} else{
				group = (Group) degreeList.get(degreeList.indexOf(degreeMap.get(Float.toString(degree))));
			}
			List<HObject> memberList = group.getMemberList();
			Dataset dataset = getDataset(rootName + "_"+ band, memberList);

			if(dataset!=null){
				dataset.init();
				long[] dimensions = dataset.getDims(); // dimensions[1] = Height, dimensions[2] = Width
				long[] selected = dataset.getSelectedDims(); //the size in each dimension to select
				long[] start = dataset.getStartDims(); // the offset of the selection
				if(dimensions.length > 2){
					throw new RuntimeException("Unsure how to handle dataset with number of dimensions = "+dimensions.length);
				}
				

				// reset the selection arrays
				for (int i=0; i<dimensions.length; i++) {
					start[i] = 0;
					selected[i] = 1;
				}
				start[0] = (yOffset < 0)? 0 : yOffset;
				start[1] = (xOffset < 0)? 0 : xOffset;
				selected[0] = (yLength <= 0)? dimensions[0] : yLength;
				selected[1] = (xLength <= 0)? dimensions[1] : xLength;
				
				Object data = dataset.getData();
				if(data instanceof byte[]){
					byte[] pixels = (byte[])data;
					imp = new ImagePlus(dataset.getFullName(), new ByteProcessor((short)selected[1], (short)selected[0], pixels));
				}else if(data instanceof short[]){
					short[] pixels = (short[])data;
					imp = new ImagePlus(dataset.getFullName(), new ShortProcessor((short)selected[1], (short)selected[0], pixels, null));
				}else if(data instanceof float[]){
					float[] pixels = (float[])data;
					imp = new ImagePlus(dataset.getFullName(), new FloatProcessor((short)selected[1], (short)selected[0], pixels, null));
				}else{
					int[] pixels = (int[])data;
					imp = new ImagePlus(dataset.getFullName(), new ColorProcessor((short)selected[1], (short)selected[0], pixels));
				}
				dataset.clear();
			}
			ExFile.close();
		} catch (HDF5Exception ex){
			ex.printStackTrace();
			IJ.log(ex.getMessage());
			IJ.log(ex.getLocalizedMessage());
		} catch (Exception ex){
			ex.printStackTrace();
		} finally{}
		return imp;
	}
	
	public void getMetaDataFromH5(H5File ExFile){
		try{
			ExFile.open();
			Map<String, String> inputAttributeMap = new HashMap<String, String>();
			DefaultMutableTreeNode ret = (DefaultMutableTreeNode) ExFile.getRootNode();
			Group root = (Group) ret.getUserObject();
			degreeList = root.getMemberList();
			orbit = 0f;
			nangles = -1;
			IJ.log("degreeList.size() = "+degreeList.size());

			for (int i = 0; i < degreeList.size(); i++){
				if (degreeList.get(i).toString().contains("degrees")){
					degreeMap.put(degreeList.get(i).toString().substring(0,degreeList.get(i).toString().lastIndexOf("degrees")),degreeList.get(i));
	
					//to find largest degree as orbit//
					if (Float.parseFloat(degreeList.get(i).toString().substring(0,degreeList.get(i).toString().lastIndexOf("degrees")))-orbit > 0){
						orbit = Float.parseFloat(degreeList.get(i).toString().substring(0,degreeList.get(i).toString().lastIndexOf("degrees")));
					}
					nangles++;
				}
			}

			IJ.log("nangles = "+nangles);
			
			IJ.log("orbit = "+orbit);

			increment = orbit/nangles;
			IJ.log("increment = "+increment);
			
			List<HObject> memberList;
			Group group = (Group) degreeList.get(degreeList.indexOf(degreeMap.get(Integer.toString(0))));
			memberList = group.getMemberList();
			Dataset dataset = getDataset(memberList.get(0).toString(),memberList);
			System.out.printf("\ndatasetName = %s\ngroup.getFullName() = %s\n", dataset.getFullName(), group.getFullName());
			if(dataset!=null){
				dataset.init();
				long[] dimensions = dataset.getDims(); // dimensions[1] = Height, dimensions[2] = Width
				long[] selected = dataset.getSelectedDims(); //the size in each dimension to select
				long[] start = dataset.getStartDims(); // the offset of the selection
				if(dimensions.length > 2){
					throw new RuntimeException("Unsure how to handle dataset with number of dimensions = "+dimensions.length);
				}
				xwidth = dataset.getWidth();
				yheight = dataset.getHeight();
			
				dataset.clear();
			}
			IJ.log("xwidth = "+ xwidth);
			IJ.log("yheight = "+ yheight);
			
			@SuppressWarnings("unchecked")
			List<Attribute> meta = group.getMetadata();
			//IJ.log(Arrays.toString(meta.toArray()));
			for (Attribute attr: meta) {
				if (attr.getType().getDatatypeClass() == Datatype.CLASS_STRING) {
					Object[] vals = (Object[]) attr.getValue();
					waveStringList = ((String)vals[0]).split(",");
					inputAttributeMap.put(attr.getName(), (String) vals[0]);
				} 
				else if (attr.getType().getDatatypeClass() ==  Datatype.CLASS_INTEGER
					&& attr.getType().getDatatypeSize() == Long.SIZE / 8) 
				{
					long[] vals = (long[]) attr.getValue();
					inputAttributeMap.put(attr.getName(), Long.toString(vals[0]));
				} else {
					throw new Exception("Don't know how to read attr " + attr.getName());
				}
			}

			for (int i = 0; i < memberList.size(); i++){
				for (int j = 0; j < waveStringList.length; j++){
					if (memberList.get(i).toString().contains(waveStringList[j])){
						waveMap.put(waveStringList[j], memberList.get(i));
					}
				}
			}
			
			
			//IJ.log(Arrays.toString(memberList.toArray())); //[All attributes from group will be composed into a list and printed as an array]
			bands = memberList.size();
			IJ.log("bands = "+bands);
		
			if (nangles%2>0){
				nangles=nangles-1;
			}
			
			ExFile.close();
		}catch(Exception ex){}finally{}
	}
	
	public void addToStack(String stackTitle, String sliceLabel, ImagePlus imp, int xwidth, int yheight){
		
		ImageProcessor ip = imp.getProcessor();
		ImagePlus oldstack = WindowManager.getImage(stackTitle);
		ImageStack newStack;
		if(oldstack!=null && oldstack.getHeight() == ip.getHeight() && oldstack.getWidth() == ip.getWidth()){
			WindowManager.setTempCurrentImage(oldstack);
			oldstack = IJ.getImage();
			newStack = oldstack.getStack();
		}
		else newStack = new ImageStack(xwidth, yheight);
		newStack.addSlice(ip);
		newStack.setSliceLabel(sliceLabel,newStack.getSize());
		//updates old stack//
		if(oldstack == null)oldstack = new ImagePlus();
		oldstack.setStack(stackTitle,newStack);
		oldstack.setSlice(newStack.getSize());
		oldstack.resetDisplayRange();
		if(newStack.getSize()==1)oldstack.show();
	}

	public void run(String arg) {
		String inputPath = getTheInputName();
			if (inputPath==null) {return;}
			else{
				String directory = inputPath.substring(0,inputPath.lastIndexOf(File.separator));
				rootName = inputPath.substring(inputPath.lastIndexOf(File.separator)+1,inputPath.lastIndexOf("."));
				IJ.log("rootName = "+rootName);

				H5File testFile = new H5File(inputPath);
				
				
				getMetaDataFromH5(testFile);
				//Finds the layers dimension of the dataset// 
				int area = xwidth*yheight;
				IJ.log("area = "+area);

				//reads all band images from 0 degrees//
				/*for (int k=0;k<bands;k++){
					ImagePlus ig = getImageDataFromH5(testFile, 0, waveStringList[k], 0, 0, xwidth, yheight);
					addToStack("Image test - 0 degrees", "#"+waveStringList[k], ig, ig.getWidth(), ig.getHeight());
				}*/
				
				//makeImageStack(testFile, waveMap, waveStringList, 0, 0, bands, 1);
				
				//ImagePlus testImage = new ImagePlus("test",testStack);
				//testImage.show();	
				
				if (directory == ""){
					return;
				} else{
					directory = directory+Prefs.separator;
				}
				IJ.log(directory);
				
				String corrDir = directory+"corr";
				corrDir = corrDir+Prefs.separator;
				File outputfile = new File(corrDir);
				if (!outputfile.exists()){outputfile.mkdir();}
				if (!outputfile.exists()){IJ.showMessage("Couldn't create requested output directory.");return;}
	
				//new dialog with recon parameters
				makeGUI();
				gd2.showDialog();
				
				if (gd2.wasCanceled()){return;}
				else {
					
					int first_wavenumber;
					int last_wavenumber;
					int first_slice; 
					int last_slice; 
					String temp = wavenumber_field.getText();
					Pattern dashPattern = Pattern.compile("-");
					String[] temparray = dashPattern.split(temp);
					first_wavenumber = Integer.valueOf(temparray[0]);
					if (temparray.length > 1){
						last_wavenumber = Integer.valueOf(temparray[1]);
					} else {
						last_wavenumber = Integer.valueOf(temparray[0]);
					}
					temp = slice_field.getText();
					String[] temparray2 = dashPattern.split(temp);
					first_slice = Integer.valueOf(temparray2[0]);
					if (temparray2.length > 1){
						last_slice = Integer.valueOf(temparray2[1]);
					} else {
						last_slice = Integer.valueOf(temparray2[0]);
					}
					// IJ.log("first slice: "+first_slice+", last slice: "+last_slice+"temparray2[1]: "+temparray2[1]);

					float cor_low = Float.parseFloat(cor_low_field.getText()); //to get a ball park of cor//
					float cor_high = Float.parseFloat(cor_high_field.getText());
					float cor_step = Float.parseFloat(cor_step_field.getText());
					float orbit = Float.parseFloat(orbit_field.getText());
					float increment = Float.parseFloat(increment_field.getText()); 
					int iterations = Integer.parseInt(iterations_field.getText());
					float beta = Float.parseFloat(beta_field.getText());
					if (cor_low == cor_high){
						steps = 1;
					} else {
						if (cor_step == 0){
							steps = 1;
						} else {
							steps = (int)((cor_high-cor_low)/cor_step+1);
						}
					}
	
					sample_file_format = file_pattern_field.getText();
	
					int[] cropdims = new int[3];
					cropdims[0] = xwidth;
					cropdims[1] = last_slice-first_slice+1;
					cropdims[2] = nangles;
	
					IJ.log("cropdims[0] = "+cropdims[0]);
					IJ.log("cropdims[1] = "+cropdims[1]);
					IJ.log("cropdims[2] = "+cropdims[2]);

					if (cropdims[2]%2>0){
						cropdims[2]=cropdims[2]-1;
					}

					int[] submitdims = new int[3];
					submitdims[0] = cropdims[0];
					submitdims[1] = nangles;
					submitdims[2] = cropdims[1];
					
					IJ.log("submitdims[0] = "+submitdims[0]);
					IJ.log("submitdims[1] = "+submitdims[1]);
					IJ.log("submitdims[2] = "+submitdims[2]);
					
					int[] finaldims = new int[3];
					finaldims[0] = submitdims[0];
					finaldims[1] = submitdims[0];
					finaldims[2] = submitdims[2];
					
					IJ.log("finaldims[0] = "+finaldims[0]);
					IJ.log("finaldims[1] = "+finaldims[1]);
					IJ.log("finaldims[2] = "+finaldims[2]);

					for (int a=first_wavenumber;a<last_wavenumber+1;a++){

						ImageStack wavenumberStack = new ImageStack(cropdims[0],cropdims[1]);
						for (int b=0;b<nangles;b++){
							IJ.log(" b*increment: "+ b*increment+", waveStringList: "+waveStringList[a]);
							ImagePlus Wave = getImageDataFromH5(testFile, b*increment, waveStringList[a], 0, 0, xwidth, yheight);
							ImageProcessor angleprocessor = Wave.getProcessor();
							IJ.log("angleprocessor[0]: "+angleprocessor.getWidth()+", angleprocessor[1]: "+angleprocessor.getHeight());

							//float[] angle_pixels_float = (float[]) angleprocessor.getPixels();
							
							Wave.close();
							/*angleFileName = String.format(directory+sample_file_format,b);
							IJ.log("Reading "+angleFileName);
							File angleFile = new File(angleFileName);
							FloatProcessor angleprocessor = new FloatProcessor(xwidth,yheight);
							float[] angle_pixels_float = new float[area];
			
							for (int x=0;x<area;x++){
								// angle_pixels_float[x] = Float.valueOf(angle_line_string[x+1].trim()).floatValue();
								// Next two lines replace above line to make absorbance data into transmission.
								//angle_pixels_float[x] = (float) Math.exp(-Float.valueOf(angle_line_string[x+1].trim()).floatValue());
							}*/
							
							//angleprocessor.setPixels(angle_pixels_float);
							
							if (vertical_box.getState() == true){
							}else{
								angleprocessor.rotate(90);
							}
							
							IJ.log("cropdims[0]: "+cropdims[0]+", cropdims[1]: "+cropdims[1]);

							angleprocessor.setRoi(0, 0, cropdims[0], cropdims[1]);
							ImageProcessor angleprocessor_crop = angleprocessor.crop();
							IJ.log("angleprocessor_crop[0]: "+angleprocessor_crop.getWidth()+", angleprocessor_crop[1]: "+angleprocessor_crop.getHeight());

							wavenumberStack.addSlice(""+b,angleprocessor_crop);
		
						}
						ImagePlus transImage = new ImagePlus("stack"+a,wavenumberStack);
						IJ.saveAs(transImage,"Tiff",String.format(corrDir+rootName+"_trans_stack_%04d.tif",a));
						Slicer the_slicer = new Slicer();
						ImagePlus resliced_trans = the_slicer.reslice(transImage);
		
						saveFLD(resliced_trans,String.format(corrDir+rootName+"_trans_%04d.fld",a));
						IJ.saveAs(resliced_trans,"Tiff",String.format(corrDir+rootName+"_trans_%04d.tif",a));
						for (int i=0;i<steps;i++){
							cor = cor_low+i*cor_step;
							delete_if_exist(corrDir+"tomo.dsc");
							delete_if_exist(String.format(corrDir+rootName+"_trans_fbp_%04d_cor"+cor+".fld",a));
							delete_if_exist(String.format(corrDir+rootName+"_trans_fbp_%04d_cor"+cor+".raw",a));
							delete_if_exist(corrDir+"tomo.wtf");
					
							recon_output = aspire_reconstruction(corrDir,rootName,"_trans_%04d.fld",a, submitdims, orbit, cor, iterations, beta);
						
							ImagePlus fbp = open_raw(recon_output,finaldims);
							fbp.show();
						}	
					}
				}
			}
	}
	

	public String parseBinaryHeader(File hdrFile){
		StringBuilder contents = new StringBuilder();
		try {
			BufferedReader input =  new BufferedReader(new FileReader(hdrFile));
			try {
				String line = null; //not declared within while loop
				while (( line = input.readLine()) != null){
					contents.append(line);
					contents.append(System.getProperty("line.separator"));
				}
			}
			finally { input.close();}
		}
		catch (IOException ex){ex.printStackTrace();}
		String hdrContents = contents.toString();
		return hdrContents;
	}

	public String parse_xrf(File xrfFile){
		StringBuilder contents = new StringBuilder();
		try {
			BufferedReader input =  new BufferedReader(new FileReader(xrfFile));
			try {
				String line = null; //not declared within while loop
				while (( line = input.readLine()) != null){
					contents.append(line);
					contents.append(System.getProperty("line.separator"));
				}
			}
			finally { input.close();}
		}
		catch (IOException ex){ex.printStackTrace();}
		String xrfContents = contents.toString();
		return xrfContents;
	}
	
	static public void saveFLD(ImagePlus imp, String path) {
		File file = new File(path);
		DataOutputStream dos = null;
		try {
			dos = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(file)));
			
			// read data:
			FileInfo fi = imp.getFileInfo();
			// HEADER: ... read all header tags and metadata		
			
			int width = (int)(imp.getWidth());
			int height = (int)(imp.getHeight());
			int slices = (int)(imp.getNSlices());			
				
			String header;
			if (slices > 1){
				header = "# AVS field file\nndim=3\ndim1="+width+"\ndim2="+height+"\ndim3="+slices+"\nnspace=3\nveclen=1\ndata=xdr_float\nfield=uniform\n\f\f";
			}
			else{
				header = "# AVS field file\nndim=2\ndim1="+width+"\ndim2="+height+"\nnspace=2\nveclen=1\ndata=xdr_float\nfield=uniform\n\f\f";
			}
			dos.writeBytes(header);
			
			for (int i=1; i<slices+1; i++) {
			imp.setSlice(i);
			float[] pixels = (float[])imp.getProcessor().getPixels();
				for (int j = 0; j < width*height; j++) {
					dos.writeFloat(pixels[j]);
				}	
			}
			
			dos.flush();
			dos.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		
	public String aspire_reconstruction(String corrDirectory,String the_file_name,String file_ending,int file_number, int[] aspire_dims, float orbit, float cor, int iterations, float beta){
		
	IJ.log("starting reconstruction");
	String dsc_file = "";
	String addon = "_trans_fbp_%04d_cor"+cor;
	String addon2 = "_trans_trpl_";
	
	try{
		dsc_file = corrDirectory+"tomo.dsc";
		FileWriter outFile = new FileWriter(dsc_file);
		PrintWriter out = new PrintWriter(outFile);

		out.println("system 2");
		out.println("nx "+aspire_dims[0]);
		out.println("ny "+aspire_dims[0]);
		out.println("nb "+aspire_dims[0]);
			out.println("na "+aspire_dims[1]);
			out.println("support ellipse 0 0 "+(aspire_dims[0]/2-2)+" "+(aspire_dims[0]/2-2));
			out.println("offset_even "+cor);
			out.println("offset_odd "+cor);
			out.println("orbit "+orbit);
			out.println("orbit_start 0");
			out.println("scale 0");
			out.println("strip_width 1");
			out.close();
			
		} catch (IOException e){
			e.printStackTrace();
		}

		//C:\\Documents and Settings\\Michael Martin\\
		String[] args = {"C:\\aspire\\input.exe","fbp2t","dsc",String.format(corrDirectory+the_file_name+"_trans_fbp_%04d_cor"+cor+".fld",file_number),"-",String.format(corrDirectory+the_file_name+file_ending,file_number),"-","1","-","0",dsc_file};
		if(IJ.isWindows() == false){
			args[0] = "/dula/software/src/aspire/i";
		}

		
		String data1;
		try{
			ProcessBuilder procbuild1 = new ProcessBuilder(args);
			procbuild1.redirectErrorStream(true);
			Process proc1 =procbuild1.start();
			BufferedReader buffrdr1 = new BufferedReader(new InputStreamReader(proc1.getInputStream()));
			while ((data1 = buffrdr1.readLine()) != null) {IJ.log(data1);}
		} catch (Throwable t){
			t.printStackTrace();
		}
/*
		String[] args4 = {"C:\\dula\\software\\aspire\\windows\\wt.exe","gen",dsc_file};
		if(IJ.isWindows() == false){
			args4[0] = "/dula/software/src/aspire/wt";
		}
		try{
			ProcessBuilder procbuild4 = new ProcessBuilder(args4);
			procbuild4.redirectErrorStream(true);
			Process proc4 =procbuild4.start();
			BufferedReader buffrdr4 = new BufferedReader(new InputStreamReader(proc4.getInputStream()));
			while ((data1 = buffrdr4.readLine()) != null) {IJ.log(data1);}
		} catch (Throwable t){
			t.printStackTrace();
        	}


		ProcessBuilder procbuild3;
		String wtf =  rootname.substring(0,rootname.lastIndexOf(Prefs.separator)+1)+"tomo.wtf";;
		String method = "@"+iterations+"@psca,od,1,raster1@"+beta+",huber,2,-,0.001,ih,1";
		String methodb = "@"+iterations+"@sage,3,raster1@"+beta+",quad,1,b2info";
        	String[] args3 = {"C:\\dula\\software\\aspire\\windows\\i.exe","trpl2",rootname+addon2+cor+".fld",args[3],rootname+"_trans.fld","-","1","-","0",wtf,"-",method};
		String[] args3b = {"C:\\dula\\software\\aspire\\windows\\i.exe","empl2",rootname+addon2+cor+".fld",args[3],rootname+"_fluor.fld","-","-",""+background,wtf,"-",methodb};
		
		if(IJ.isWindows() == false){
			args3[0] = "/dula/software/src/aspire/i";
			args3b[0] = "/dula/software/src/aspire/i";
		}

		
		try{
			if (transmission == true){
				procbuild3 = new ProcessBuilder(args3);
			}else{
				procbuild3 = new ProcessBuilder(args3b);
			}
			procbuild3.redirectErrorStream(true);
			Process proc3 =procbuild3.start();
			BufferedReader buffrdr3 = new BufferedReader(new InputStreamReader(proc3.getInputStream()));
			while ((data1 = buffrdr3.readLine()) != null) {IJ.log(data1);}
		} catch (Throwable t){
			t.printStackTrace();
        	}
*/
        	
		//String output_name = rootname+addon2+cor+".raw";
		String output_name = String.format(corrDirectory+the_file_name+"_trans_fbp_%04d_cor"+cor+".raw",file_number);
		//String[] args2 = {"C:\\dula\\software\\aspire\\windows\\op.exe","conv",output_name,rootname+addon2+cor+".fld","raw"};
		String[] args2 = {"C:\\aspire\\output.exe","conv",output_name,String.format(corrDirectory+the_file_name+"_trans_fbp_%04d_cor"+cor+".fld",file_number),"raw"};
		if(IJ.isWindows() == false){
			args2[0] = "/dula/software/src/aspire/op";
		}
		
		try{
			ProcessBuilder procbuild2 = new ProcessBuilder(args2);
			procbuild2.redirectErrorStream(true);
			Process proc2 =procbuild2.start();
			BufferedReader buffrdr2 = new BufferedReader(new InputStreamReader(proc2.getInputStream()));
			while ((data1 = buffrdr2.readLine()) != null) {IJ.log(data1);}
		} catch (Throwable t){
			t.printStackTrace();
		}
		
		
		return output_name;
		}

	public ImagePlus open_raw(String filename,int[] dims){
		FileInfo fi = new FileInfo();
		fi.fileFormat = fi.RAW;
		fi.fileName = filename.substring(filename.lastIndexOf(Prefs.separator)+1,filename.length());
		fi.directory = filename.substring(0,filename.lastIndexOf(Prefs.separator));
		fi.width = dims[0];
		fi.height = dims[1];
		fi.offset = 0;
		fi.nImages = dims[2];
		fi.gapBetweenImages = 0;
		fi.intelByteOrder = false;
		fi.whiteIsZero = false;
		fi.fileType = FileInfo.GRAY32_FLOAT;
		
		FileOpener fo = new FileOpener(fi);
		ImagePlus fbp = fo.open(false);
		return fbp;
		
	}

	public void delete_if_exist(String filename){
		File alreadyexist = new File(filename);
		if (alreadyexist.exists() == true){
			alreadyexist.delete();
		}
	}
}// end of ir_tomo class

class AttributeMap<Key, Value> {
	private Map<Key, Value> map;
	
	public AttributeMap(Map<Key, Value> map) {
		this.map = map;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		Iterator<Entry<Key, Value>> iter = map.entrySet().iterator();
		while (iter.hasNext()) {
			Entry<Key, Value> entry = iter.next();
			sb.append(entry.getKey());
			sb.append('=').append('"');
			sb.append(entry.getValue());
			sb.append('"');
			if (iter.hasNext()) {
				sb.append(',').append(' ');
			}
		}
		return sb.toString();
		
	}
}
